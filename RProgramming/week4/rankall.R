rankall <- function(outcome, num = "best") {
	# read outcome data
	data <- read.csv("outcome-of-care-measures.csv")
	result <- data.frame(hospital=character(), state=character(), stringsAsFactors=FALSE)
	state_names = levels(data$State)
	data <- read.csv("outcome-of-care-measures.csv", colClasses = "character")

	for (state in state_names) {
		state_outcome <- data[data$State==state,]

		# check if state is valid
		if (nrow(state_outcome) <= 0) {
			stop('invalid state')
		}

		index <- if (outcome == 'heart attack') {
			11
		} else if (outcome == 'heart failure') {
			17
		} else if (outcome == 'pneumonia') {
			23
		} else {
			stop('invalid outcome')		
		}



		state_outcome[[index]] <- as.numeric(state_outcome[[index]])
		outcomes <- state_outcome[!is.na(state_outcome[index]),]

		sorted_outcomes <- outcomes[order(outcomes[index], outcomes$Hospital.Name),]


		rank = if (num == "best") {
			sorted_outcomes$Hospital.Name[1]
		} else if (num == "worst") {
			sorted_outcomes$Hospital.Name[nrow(sorted_outcomes)]
		} else {
			sorted_outcomes$Hospital.Name[as.integer(num)]
		}
		result <- rbind(result, data.frame(hospital=rank, state=state))
	}
	result
}
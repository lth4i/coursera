best <- function(state, outcome) {
	# read outcome data
	data <- read.csv("outcome-of-care-measures.csv", colClasses = "character")

	state_outcome <- data[data$State==state,]

	# check if state is valid
	if (nrow(state_outcome) <= 0) {
		stop('invalid state')
	}

	index <- if (outcome == 'heart attack') {
		11
	} else if (outcome == 'heart failure') {
		17
	} else if (outcome == 'pneumonia') {
		23
	} else {
		stop('invalid outcome')		
	}

	# outcomes <- state_outcome[index]
	state_outcome[[index]] <- as.numeric(state_outcome[[index]])
	outcomes <- state_outcome[!is.na(state_outcome[index]),]

	sorted_outcomes <- outcomes[order(outcomes[index], outcomes$Hospital.Name),]

	sorted_outcomes$Hospital.Name[1]
}
import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.In;

import java.util.ArrayList;
import java.util.HashMap;

public class SAP {

    private class SAPResult {
        private final int root;
        private final int length;
        private SAPResult(int root, int length) {
            this.root = root;
            this.length = length;
        }
    }

    private Digraph g;

    // constructor takes a digraph (not necessarily a DAG)
    public SAP(Digraph G) {
        g = new Digraph(G.V());
        for (int i = 0; i < G.V(); i++) {
            for (int j : G.adj(i)) {
                g.addEdge(i, j);
            }
        }
    }

    // length of shortest ancestral path between v and w; -1 if no such path
    public int length(int v, int w) {
        if (v < 0 || v >= g.V() || w < 0 || w >= g.V()) {
            throw new java.lang.IndexOutOfBoundsException();
        } else if (v == w) {
            return 0;
        }
        SAPResult r = findSAP(v, w);
        // System.out.println(v + "\t" + w + "\t" + r.length);
        return r.length;
    }

    // a common ancestor of a and b that participates in a shortest ancestral path; -1 if no such path
    public int ancestor(int a, int b) {
        if (a < 0 || a >= g.V() || b < 0 || b >= g.V()) {
            throw new java.lang.IndexOutOfBoundsException(a + " " + b + " " + g.V());
        }

        if (a == b) {
            return a;
        }
        return findSAP(a, b).root;
    }

    private boolean isAdj(int s, int d) {
        Iterable<Integer> adj = g.adj(s);
        for (int n : adj) {
          if (n == d) {
            return true;
          }
        }
        return false;
    }

    // length of shortest ancestral path between any vertex in v and any vertex in w; -1 if no such path
    public int length(Iterable<Integer> v, Iterable<Integer> w) {
        if (v == null || w == null) throw new java.lang.NullPointerException();
        int dist = Integer.MAX_VALUE;
        for (int v1 : v) {
            for (int w1 : w) {
                int d = length(v1, w1);
                // System.out.println(v1 + "\n" + w1 + "\t" + d);
                if (d > -1 && d < dist) {
                    dist = d;
                }
            }
        }
        return dist;
    }

    // a common ancestor that participates in shortest ancestral path; -1 if no such path
    public int ancestor(Iterable<Integer> v, Iterable<Integer> w) {
        if (v == null || w == null) throw new java.lang.NullPointerException();
        int dist = Integer.MAX_VALUE;
        int a = -1;
        int b = -1;
        for (int v1 : v) {
            for (int w1 : w) {
                int d = length(v1, w1);
                if (d > -1 && d < dist) {
                    dist = d;
                    a = v1;
                    b = w1;
                }
            }
        }
        if (a != -1 && b != -1) {
            return ancestor(a, b);
        }
        return -1;
    }

    private SAPResult findSAP(int a, int b) {
        HashMap<Integer, ArrayList<Integer>> aLevels = buildLevels(a);
        HashMap<Integer, ArrayList<Integer>> bLevels = buildLevels(b);
        // printLevels(aLevels);
        // printLevels(bLevels);
        return checkLevels(aLevels, bLevels);
    }

    private void printLevels(HashMap<Integer, ArrayList<Integer>> levels) {
        for (int i = 0; i < levels.size(); i++) {
            System.out.println(i + "\t" + levels.get(i));
        }
    }

    private HashMap<Integer, ArrayList<Integer>> buildLevels(int n) {
        HashMap<Integer, ArrayList<Integer>> levels = new HashMap<Integer, ArrayList<Integer>>();
        ArrayList<Integer> nextLevel = new ArrayList<Integer>();
        nextLevel.add(n);
        levels.put(0, nextLevel);
        int counter = 0;
        boolean[] marked = new boolean[g.V()];
        while (true) {
            ArrayList<Integer> currentLevel = levels.get(counter);
            nextLevel = new ArrayList<Integer>();

            for (int v : currentLevel) { // add nodes of the next level
                Iterable<Integer> adj = g.adj(v);
                for (int a : adj) {
                    if (!marked[a]) {
                        nextLevel.add(a);
                        marked[a] = true;
                    }
                }
            }

            if (nextLevel.isEmpty()) { // there is no new level
                return levels;
            } else {
                counter++;
                levels.put(counter, nextLevel);
            }
        }
    }

    private SAPResult checkLevels(HashMap<Integer, ArrayList<Integer>> aLevels,
        HashMap<Integer, ArrayList<Integer>> bLevels) {

        int minLength = Integer.MAX_VALUE;
        int root = -1;

        // compare level by level
        for (int aCounter = 0; aCounter < aLevels.size(); aCounter++) {
            ArrayList<Integer> aLevel = aLevels.get(aCounter);
            for (int bCounter = 0; bCounter < bLevels.size(); bCounter++) {
                ArrayList<Integer> bLevel = bLevels.get(bCounter);
                for (int a : aLevel) {
                    for (int b : bLevel) {
                        // System.out.println(a + "\t" + b);
                        if (a == b) {
                            // System.out.println("Root " + a);
                            int length = aCounter + bCounter;
                            if (length < minLength) {
                                minLength = length;
                                root = a;
                            }
                        }
                    }
                }
            }
        }

        if (root == -1) {
            return new SAPResult(-1, -1);
        } else {
            return new SAPResult(root, minLength);
        }
    }

    private SAPResult checkNewLevel(ArrayList<Integer> newLevel, 
        HashMap<Integer, ArrayList<Integer>> levels) {
        for (int counter = 0; counter < levels.size(); counter++) {
            ArrayList<Integer> level = levels.get(counter);
            for (int i1 : level) {
                for (int i2 : newLevel) {
                    if (i1 == i2) {
                        return new SAPResult(i1, counter);
                    }
                }
            }
        }
        return new SAPResult(-1, -1);
    }

    // do unit testing of this class
    public static void main(String[] args) {
        In in = new In(args[0]);
        Digraph G = new Digraph(in);
        SAP sap = new SAP(G);
        while (!StdIn.isEmpty()) {
            int v = StdIn.readInt();
            int w = StdIn.readInt();
            int length   = sap.length(v, w);
            int ancestor = sap.ancestor(v, w);
            StdOut.printf("length = %d, ancestor = %d\n", length, ancestor);
        }
    }
}
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

public class Outcast {

    private final WordNet wordnet;

    public Outcast(WordNet wordnet) {
        this.wordnet = wordnet;
    }

    public String outcast(String[] nouns) {
        if (nouns == null) throw new java.lang.NullPointerException();

        int maxDistance = Integer.MIN_VALUE;
        String n = null;
        for (int i1 = 0; i1 < nouns.length; i1++) {
            String n1 = nouns[i1];
            int nounDistance = 0;
            for (int i2 = 0; i2 < nouns.length; i2++) {
                if (i1 == i2) {
                    continue;
                }
                String n2 = nouns[i2];
                int distance = wordnet.distance(n1, n2);
                nounDistance += distance;
            }
            // System.out.println(n1 + " " + nounDistance);
            if (nounDistance > maxDistance) {
                maxDistance = nounDistance;
                n = n1;
            }
        }
        return n;
    }

    public static void main(String[] args) {
        WordNet wordnet = new WordNet(args[0], args[1]);
        Outcast outcast = new Outcast(wordnet);
        for (int t = 2; t < args.length; t++) {
            In in = new In(args[t]);
            String[] nouns = in.readAllStrings();
            StdOut.println(args[t] + ": " + outcast.outcast(nouns));
        }
    }
}
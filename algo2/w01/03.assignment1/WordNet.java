import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Digraph;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.LinkedList;

public class WordNet {

    private final Digraph g;
    private final HashMap<String, ArrayList<Integer>> words = new HashMap<String, ArrayList<Integer>>();
    private final HashMap<Integer, String> ids = new HashMap<Integer, String>();
    private final int v;
    private final SAP sap;

    // constructor takes the name of the two input files
    public WordNet(String synsets, String hypernyms) {
        if (synsets == null || hypernyms == null) {
          throw new java.lang.NullPointerException();
        }

        In synsetsIn = new In(synsets);
        In hypernymsIn = new In(hypernyms);

        // create a map
        String[] lines = synsetsIn.readAllLines();
        v = lines.length;
        g = new Digraph(v);

        for (String line : lines) {
          String[] tokens = line.split(",");
          int id = Integer.parseInt(tokens[0]);
          String[] nouns = tokens[1].split(" ");
          ids.put(id, tokens[1]);
          for (int i = 0; i < nouns.length; i++) {
            String n = nouns[i];
            if (words.containsKey(n)) {
              words.get(n).add(id);
            } else {
              ArrayList<Integer> nIds = new ArrayList<Integer>();
              nIds.add(id);
              words.put(n, nIds);
            }
          }
        }

        // for (int id : ids.keySet()) System.out.println(id + " " + ids.get(id));

        // add link to map
        lines = hypernymsIn.readAllLines();
        for (String line : lines) {
          String[] tokens = line.split(",");
          int d = Integer.parseInt(tokens[0]);
          for (int i = 1; i < tokens.length; i++) {
            int s = Integer.parseInt(tokens[i]);
            g.addEdge(d, s);
          }
        }


        // there must be a noun which is not a hypernym
        boolean isRooted = false;
        for (ArrayList<Integer> wIds : words.values()) {
          for (int e : wIds) {
            if (g.outdegree(e) == 0) {
              if (!isRooted) {
                isRooted = true;
              } else { // two root
                throw new java.lang.IllegalArgumentException();
              }
            }
            checkLoop(e);
            // System.out.println();
          }
        }
        if (!isRooted) throw new java.lang.IllegalArgumentException();

        sap = new SAP(g);
    }

    private void checkLoop(int s) {
        boolean[] marked = new boolean[g.V()];
        LinkedList<Integer> q = new LinkedList<Integer>();
        q.add(s);
        boolean first = false;

        while (!q.isEmpty()) {
            int current = q.poll();
            // System.out.print(current + " ");
            if (current == s) {
              if (!first) {
                first = true;
              } else {
                throw new java.lang.IllegalArgumentException();
              }
            }

            Iterable<Integer> adj = g.adj(current);
            for (int i : adj) {
                if (!marked[i]) {
                    q.add(i);
                    marked[i] = true;
                }
            }
        }
    }

    // returns all WordNet nouns
    public Iterable<String> nouns() {
        return words.keySet();
    }

    // is the word a WordNet noun?
    public boolean isNoun(String word) {
      if (word == null) throw new java.lang.NullPointerException();
        return words.containsKey(word);
    }

    // distance between nounA and nounB (defined below)
    public int distance(String nounA, String nounB) {
        if (nounA == null || nounB == null) throw new java.lang.NullPointerException();
        else if (!isNoun(nounA)) throw new java.lang.IllegalArgumentException(nounA);
        else if (!isNoun(nounB)) throw new java.lang.IllegalArgumentException(nounB);

        if (nounA.equals(nounB)) {
          return 0;
        }

        ArrayList<Integer> a = words.get(nounA);
        ArrayList<Integer> b = words.get(nounB);
        // System.out.println(a + " " + b + " " + nounA + " " + nounB);

        return sap.length(a, b);
    }

    // a synset (second field of synsets.txt) that is the common ancestor of nounA and nounB
    // in a shortest ancestral path (defined below)
    public String sap(String nounA, String nounB) {
        if (nounA == null || nounB == null) throw new java.lang.NullPointerException();
        else if (!isNoun(nounA) || !isNoun(nounB)) throw new java.lang.IllegalArgumentException();

        // if (nounA.equals(nounB)) {
        //   int id = words.get(nounA);
        //   return ids.get(id);
        // }

        ArrayList<Integer> a = words.get(nounA);
        ArrayList<Integer> b = words.get(nounB);

        int ancestor = sap.ancestor(a, b);
        // System.out.println(ancestor);
        return ids.get(ancestor);
    }

    // do unit testing of this class
    public static void main(String[] args) {
      WordNet wn = new WordNet("synsets6.txt", "hypernyms6InvalidCycle+Path.txt");
      String sap = wn.sap("SOD", "SOD");
      System.out.println(sap);
    }
}

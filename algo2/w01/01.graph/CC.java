public class CC {
  private final Graph g;
  private final boolean[] marked;
  private final int[] id;
  private int count;

  CC(Graph g) {
    this.g = g;
    marked = new boolean[this.g.V()];
    id = new int[this.g.V()];
    count = 0;

    for (int i = 0; i < this.g.V(); i++) {
      marked[i] = false;
    }

    for (int i = 0; i < this.g.V(); i++) {
      if (!marked[i]) {
        dfs(i);
        count++;
      }
    }
  }

  private void dfs(int v) {
    marked[v] = true;
    id[v] = count;
    for (int w : g.adj(v)) {
      if (!marked[w]) {
        dfs(w);
      }
    }
  }

  public boolean connected(int v, int w) {
    return id[v] == id[w];
  }

  public int count() { // number of connected components
    return count;
  }

  public int id(int v) { // component identifier for v
    return id[v];
  }
}

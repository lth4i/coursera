import java.util.LinkedList;

public class BreathFirstPaths extends Paths {

  public BreathFirstPaths(Graph g, int s) {
    super(g, s);
    bfs();
  }

  private void bfs() {
    LinkedList<Integer> q = new LinkedList<Integer>();
    q.add(s);

    while(!q.isEmpty()) {
      int current = q.poll();

      Iterable<Integer> adj = g.adj(current);
      for (int i : adj) {
        if (!marked[i]) {
          q.add(i);
          marked[i] = true;
          edgeTo[i] = current;
        }
      }
    }
  }

  public static void main (String[] args) {
    Graph g = new Graph(5);
    g.addEdge(0, 1);
    g.addEdge(2, 1);
    g.addEdge(2, 3);
    g.addEdge(3, 4);
    g.addEdge(2, 4);

    BreathFirstPaths p = new BreathFirstPaths(g, 0);
    Iterable<Integer> pt = p.pathTo(4);
    for (int i : pt) System.out.println(i);
    pt = p.pathTo(0);
    for (int i : pt) System.out.println(i);
  }
}

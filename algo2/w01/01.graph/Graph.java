import java.util.ArrayList;

public class Graph {

  private final int V;
  private final ArrayList<ArrayList<Integer>> adj;
  private int E = 0;

  Graph (int V) {
    this.V = V;
    adj = new ArrayList<ArrayList<Integer>>();
    for (int i = 0; i < V; i++) {
      adj.add(new ArrayList<Integer>());
    }
  }

  // Graph (In in) {}

  public void addEdge(int v, int w) {
    adj.get(v).add(w);
    adj.get(w).add(v);
    E++;
  }

  public Iterable<Integer> adj(int v) {
    return adj.get(v);
  }

  public int V() {
    return V;
  }

  public int E() {
    return E;
  }

  public int degree(int v) {
    return adj.get(v).size();
  }

  public String toString() {
    return "";
  }
}

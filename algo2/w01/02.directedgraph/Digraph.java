import java.util.ArrayList;

public class Digraph {
  private final int v;
  private int e;

  private final ArrayList<ArrayList<Integer>> adj;

  public Digraph (int v) {
    this.v = v;
    this.e = 0;

    adj = new ArrayList<ArrayList<Integer>>();
    for (int i = 0; i < this.v; i++) {
      adj.add(new ArrayList<Integer>());
    }
  }

  public void addEdge(int v, int w) {
    e++;
    adj.get(v).add(w);
  }

  public Iterable<Integer> adj (int v) {
    return adj.get(v);
  }

  public int v() {
    return v;
  }

  public int e() {
    return e;
  }

  public Digraph reverse() {
    Digraph rGraph = new Digraph(this.v);
    for (int i = 0; i < this.v; i++) {
      Iterable<Integer> vAdj = adj(i);
      for (int j : vAdj) {
        rGraph.addEdge(j, i);
      }
    }
    return rGraph;
  }
}

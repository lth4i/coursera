public class SCC {
	private boolean[] marked;
	private int[] id;
	private int count;

	public SCC(Digraph g) {
		marked = new boolean[g.v()];
		id = new int[g.v()];

		DepthFirstOrder dfs = new DepthFirstOrder(g.reverse());

		for (int v : dfs.reversePost()) {
			if(!marked[v]) {
				dfs(g, v);
				count++;
			}
		}
	}

	private void dfs(Digraph g, int v) {
		marked[v] = true;
		id[v] = count;
		for (int w : g.adj(v)) {
			if (!marked[w]) {
				dfs(g, v);
			}
		}
	}

	public boolean stronglyConnected(int v, int w) {
		return id[v] == id[w];
	}
}
import java.util.Stack;

public class DepthFirstOrder {
	private boolean[] marked;
	private Stack<Integer> reversePost;

	public DepthFirstOrder(Digraph g) {
		reversePost = new Stack<Integer>();
		marked = new boolean[g.v()];

		for (int i = 0; i < g.v(); i++) {
			if (!marked[i]) {
				dfs(g, i);
			}
		}
	}

	private void dfs(Digraph g, int v) {
		marked[v] = true;
		for (int w : g.adj(v)) {
			if (!marked[w]) dfs(g, w);
		}
		reversePost.push(v);
	}

	public Iterable<Integer> reversePost() {
		return this.reversePost;
	}
}
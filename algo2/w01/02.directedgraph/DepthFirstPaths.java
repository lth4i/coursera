public class DepthFirstPaths extends Paths {
  public DepthFirstPaths(Digraph g, int s) {
    super(g, s);
    dfs(this.s);
  }

  private void dfs(int v) {
    marked[v] = true;
    for (int w : g.adj(v)) {
      if (!marked[w]) {
        dfs(w);
        edgeTo[w] = v;
      }
    }
  }

  public static void main (String[] args) {
    Digraph g = new Digraph(5);
    g.addEdge(0, 1);
    g.addEdge(2, 1);
    g.addEdge(2, 3);
    g.addEdge(3, 4);
    g.addEdge(2, 4);

    DepthFirstPaths p = new DepthFirstPaths(g, 0);
    Iterable<Integer> pt = p.pathTo(4);
    for (int i : pt) System.out.println(i);
    pt = p.pathTo(0);
    for (int i : pt) System.out.println(i);
  }
}

import java.util.Stack;

public abstract class Paths {

  protected final Digraph g;
  protected final int s; // source
  protected boolean[] marked;
  protected int[] edgeTo;

  public Paths(Digraph g, int s) {
    this.g = g;
    this.s = s;
    marked = new boolean[this.g.V()];
    edgeTo = new int[this.g.V()];
    for (int i = 0; i < this.g.V(); i++) {
      marked[i] = false;
      edgeTo[i] = -1;
    }
    marked[s] = true;
    edgeTo[s] = s;
  }

  public boolean hasPathTo(int v) {
    return marked[v];
  }

  public Iterable<Integer> pathTo(int v) {
    if (!hasPathTo(v)) return new Stack<Integer>();

    Stack<Integer> path = new Stack<Integer>();
    for (int i = v; i != s; i = edgeTo[i]) {
      path.push(i);
    }
    path.push(s);
    return path;
  }
}

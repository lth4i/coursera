public class NFA {

  private Digraph buildEpsilonTransitionDigraph() {
    Digraph G = new Diagraph(M + 1);
    LinkedList<Integer> ops = new LinkedList<Integer>();
    for (int i = 0; i < M; i++) {
      int lp = i;

      if (re[i] == '(' || re[i] == '|') ops.add(i);

      else if (re[i] == ')') {
        int or = ops.poll();
        if (re[or] == '|') {
          lp = ops.poll();
          G.addEdge(lp, or + 1);
          G.addEdge(or, i);
        } else lp = or;
      }

      if (i < M - 1 && re[i + 1] == '*') {
        G.addEdge(lp, i + 1);
        G.addEdge(i + 1, lp);
      }

      if (re[i] == '(' || re[i] == '*' || re[i] == ')') G.addEdge(i, i + 1);
    }

    return G;
  }

  public boolean recognises(String txt) {
    // state reachable from start by epsilon-transition
    ArrayList<Integer> pc = new ArrayList<Integer>();
    DirectedDFS dfs = new DirectedDFS(G, 0);
    for (int v = 0; v < G.V(); v++) {
      if (dfs.marked(v)) {
        pc.add(v);
      }
    }

    for (int i = 0; i < txt.length(); i++) {
      // state reachable after scanning past txt.charAt(i)
      ArrayList<Integer> match = new ArrayList<Integer>();
      for (int v : pc) {
        if (v == M) continue;
        if ((re[v] == txt.charAt(i)) || re[v] == '.') {
          match.add(v + 1);
        }
      }

      // follow epsilon-transition
      dfs = new DirectedDFS(G, match);
      pc = new ArrayList<Integer>();

      for (int v = 0; v < G.V(); v++) {
        if (dfs.marked(v)) pc.add(v);
      }
    }

    for (int v : pc) { // accept if can end in state M
      if (v == M) return true;
    }

    return false;
  }
}

import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;

public class MoveToFront {

  private static final int R = 256;

  // apply move-to-front encoding, reading from standard input and writing to standard output
  public static void encode() {
    char[] positions = new char[R];
    for (char i = 0; i < R; i++) {
      // System.out.println(i);
      positions[i] = i;
    }
    while (!BinaryStdIn.isEmpty()) {
      char b = BinaryStdIn.readChar();
      if (positions[0] == b) {
        BinaryStdOut.write((char) 0);
        BinaryStdOut.flush();
      } else {
        char c = positions[0];
        positions[0] = b;
        for (int i = 1; i < R; i++) {
          char temp = positions[i];
          positions[i] = c;
          c = temp;
          if (c == b) {
            BinaryStdOut.write((char) i);
            BinaryStdOut.flush();
            break;
          }
        }
      }
    }
  }

  // apply move-to-front decoding, reading from standard input and writing to standard output
  public static void decode() {
    char[] positions = new char[R];
    for (char i = 0; i < R; i++) {
      // System.out.println(i);
      positions[i] = i;
    }
    while (!BinaryStdIn.isEmpty()) {
      char b = BinaryStdIn.readChar();
      char c = positions[b];

      BinaryStdOut.write(c);
      BinaryStdOut.flush();

      char temp = positions[0];
      positions[0] = c; // move the selected char to the top of an array

      for (int i = 1; i <= b; i++) {
        char temp1 = positions[i];
        positions[i] = temp;
        temp = temp1;
      }
    }
  }

  // if args[0] is '-', apply move-to-front encoding
  // if args[0] is '+', apply move-to-front decoding
  public static void main(String[] args) {
    if (args.length == 1) {
      if (args[0].trim().equals("-")) encode();
      else if (args[0].trim().equals("+")) decode();
    }
  }
}

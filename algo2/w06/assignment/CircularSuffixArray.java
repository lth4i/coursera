public class CircularSuffixArray {
  private final int[] array;
  private final int length;

  public CircularSuffixArray(String s) {
    if (s == null) throw new java.lang.NullPointerException();

    char[] characters = s.toCharArray();
    length = s.length();
    array = sort(characters, length);

    // for (int i = 0; i < l; i++) {
    //   System.out.println(i + "\t" + a[i] + "\t" + createString(a[i], characters));
    // }
  }

  // private void printArray(int[] a, char[] characters) {
  //   for (int i = 0; i < l; i ++) {
  //     System.out.println(i + "\t" + a[i] + "\t" + createString(a[i], characters));
  //   }
  //   System.out.println();
  // }

  private String createString(int i, char[] characters) {
    int l = characters.length;
    char[] newA = new char[l];
    System.arraycopy(characters, i, newA, 0, l - i);
    System.arraycopy(characters, 0, newA, l - i, i);
    return new String(newA);
  }

  private int[] sort(char[] characters, int l) {
    int[] a = new int[l];
    int[] aux = new int[l];
    for (int i = 0; i < l; i++) {
      a[i] = i;
      // aux[i] = i;
    }
    // printArray(a, characters);
    // System.out.println(l);
    sort(a, aux, 0, l - 1, characters, l);

    return a;
  }

  private void sort(int[] a, int[] aux,
    int lo, int hi,
    final char[] characters, final int l) {
    // System.out.println(lo + "\t" + hi);
    if (lo >= hi) return;

    int mid = (lo + hi) / 2;
    sort(a, aux, lo, mid, characters, l); // sort first half
    sort(a, aux, mid + 1, hi, characters, l); // sort second half

    merge(a, aux, lo, mid, hi, characters, l); // merge
    // printArray(a, characters);
  }

  private void merge(int[] a, int[] aux,
    int lo, int mid, int hi,
    final char[] characters, final int l) {

    // copy all items from the main array to the auxilary one
    for (int k = lo; k <= hi; k++) {
      aux[k] = a[k];
    }

    int i = lo;
    int j = mid + 1;

    for (int k = lo; k <= hi; k++) {
      if (i > mid) {
        // all the first half is added, keep adding the remaining second half
        a[k] = aux[j++];
      } else if (j > hi) {
        // all the second half is added, keep adding the remaining first half
        a[k] = aux[i++];
      } else if (compare(aux[i], aux[j], characters, l) > 0) {
        // if an item in the first half  is greater than another item in the second half
        // put an item in the second half in
        a[k] = aux[j++];
      } else {
        // otherwise, i.e. an item in the first half is not greater than another one on the second half
        // put an item in the first half in
        a[k] = aux[i++];
      }
    }

  }

  private int compare(int i1, int i2, char[] characters, int l) {
    int flag = 0;
    for (int i = 0; i < l; i++) {
      int index1 = (i1 + i) % l;
      int index2 = (i2 + i) % l;
      if (characters[index1] > characters[index2]) {
        flag = 1;
        break;
      }
      if (characters[index1] < characters[index2]) {
        flag = -1;
        break;
      }
    }
    return flag;
  }

  public int length() {
    return length;
  }

  public int index(int i) {
    return array[i];
  }

  public static void main(String[] args) {
    String s = "ABRACADABRA!";
    // s = "defabc!";
    CircularSuffixArray c = new CircularSuffixArray(s);
    for (int i = 0; i <= s.length(); i++) {
      System.out.println(i + "\t" + c.index(i));
    }
  }
}

// import edu.princeton.cs.algs4.StdIn;
// import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.BinaryStdOut;
import edu.princeton.cs.algs4.BinaryStdIn;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

public class BurrowsWheeler {
  // apply Burrows-Wheeler encoding, reading from standard input and writing to standard output
  public static void encode() {
    String s = BinaryStdIn.readString();
    int l = s.length();
    CircularSuffixArray a = new CircularSuffixArray(s);
    int first = -1;
    char[] characters = s.toCharArray();
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < l; i++) {
      if (a.index(i) == 0) first = i;
      int index = a.index(i) - 1;
      if (index == -1) index = l - 1;
      sb.append(characters[index]);
    }
    BinaryStdOut.write(first);
    BinaryStdOut.write(sb.toString());
    BinaryStdOut.flush();
  }

  // apply Burrows-Wheeler decoding, reading from standard input and writing to standard output
  public static void decode() {
    int first = BinaryStdIn.readInt();

    int current = 0;
    StringBuilder sb = new StringBuilder();
    HashMap<Character, LinkedList<Integer>> charToPositions
      = new HashMap<Character, LinkedList<Integer>>();
    while (!BinaryStdIn.isEmpty()) {
      char c = BinaryStdIn.readChar();
      sb.append(c);
      if (charToPositions.get(c) == null) {
        charToPositions.put(c, new LinkedList<Integer>());
      }
      charToPositions.get(c).add(current);
      current++;
    }

    String s = sb.toString();
    char[] sorted = s.toCharArray();
    Arrays.sort(sorted);

    int l = s.length();

    int[] next = new int[l];
    current = 0;
    while (current < l) {
      char c = sorted[current];
      LinkedList<Integer> positions = charToPositions.get(c);
      int temp = current + positions.size();
      int size = positions.size();
      for (int i = 0; i < size; i++) {
        next[current + i] = positions.poll();
      }
      current = temp;
    }

    StringBuilder sb1 = new StringBuilder();
    current = first;
    for (int i = 0; i < l; i++) {
      sb1.append(sorted[current]);
      current = next[current];
    }
    BinaryStdOut.write(sb1.toString());
    BinaryStdOut.flush();

  }

  // if args[0] is '-', apply Burrows-Wheeler encoding
  // if args[0] is '+', apply Burrows-Wheeler decoding
  public static void main(String[] args) {
    if (args.length == 1) {
      if (args[0].equals("-")) {
        encode();
      } else if (args[0].equals("+")) {
        decode();
      }
    }
  }
}

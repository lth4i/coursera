public class FlowEdge {

  private final int v, w;
  private final double capacity;
  private double flow;

  FlowEdge(int v, int w, double capacity) {
    this.v = v;
    this.w = w;
    this.capacity = capacity;
    this.flow = 0;
  }

  int from() {
    return v;
  }

  int to() {
    return w;
  }

  int other(int v) {
    if (v ==  this.v) {
      return w;
    } else {
      return this.v;
    }
  }

  double capacity() {
    return this.capacity;
  }

  double flow() {
    return this.flow;
  }

  double residualCapacityTo(int v) {
    if (v == this.v) { // backward
      return flow;
    } else { // forward
      return capacity - flow;
    }
  }

  void addResidualFlowTo(int v, double delta) {
    if (v == v) {
      flow -= delta;
    } else {
      flow += delta;
    }
  }
}

import java.util.ArrayList;

public class FlowNetwork {

  private final int V;
  private int E;
  private ArrayList<FlowEdge>[] adj;

  FlowNetwork(int V) {
    this.V = V;
    this.E = 0;

    adj = (ArrayList<FlowEdge>[]) new ArrayList[V];
    for (int v = 0; v < V; v++) {
      adj[v] = new ArrayList<FlowEdge>();
    }
  }

  void addEdge(FlowEdge e) {
    int v = e.from();
    int w = e.to();
    adj[v].add(e);
    adj[w].add(e);
    E++;
  }

  Iterable<FlowEdge> adj(int v) {
    return adj[v];
  }

  // Iterable<FlowEdge> edges()

  int V() {
    return V;
  }

  int E() {
    return E;
  }
}

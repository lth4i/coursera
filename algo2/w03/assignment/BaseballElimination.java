import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;
import java.util.HashMap;
import java.util.LinkedList;

public class BaseballElimination {

  private final int num;
  // private final String[] names;
  private final int[] wins, losses, lefts;
  private final String[] names;
  private final int[][] againsts;
  private final HashMap<String, Integer> teams = new HashMap<String, Integer>();
  private final HashMap<Integer, Iterable<String>> termination;
  public BaseballElimination(String filename) {
    In in = new In(filename);

    // create a map
    String[] lines = in.readAllLines();

    if (lines.length == 0) {
      throw new IllegalArgumentException();
    }

    num = Integer.parseInt(lines[0]);
    // names = new String[num];
    wins = new int[num];
    losses = new int[num];
    lefts = new int[num];
    againsts = new int[num][num];
    names = new String[num];

    for (int i = 0; i < num; i++) {
      String line = lines[i + 1].trim();
      String[] tokens = line.split("\\s+");
      names[i] = tokens[0];
      teams.put(tokens[0], i);
      wins[i] = Integer.parseInt(tokens[1]);
      losses[i] = Integer.parseInt(tokens[2]);
      lefts[i] = Integer.parseInt(tokens[3]);
      for (int j = 4; j < 4 + num; j++) {
        againsts[i][j - 4] = Integer.parseInt(tokens[j]);
      }
    }

    termination = new HashMap<Integer, Iterable<String>>();
  }

  public int numberOfTeams() {
    return num;
  }

  public Iterable<String> teams() {
    return teams.keySet();
  }

  public int wins(String team) {
    if (teams.containsKey(team)) {
      return wins[teams.get(team)];
    }
    throw new IllegalArgumentException();
  }

  public int losses(String team) {
    if (teams.containsKey(team)) {
      return losses[teams.get(team)];
    }
    throw new IllegalArgumentException();
  }

  public int remaining(String team) {
    if (teams.containsKey(team)) {
      return lefts[teams.get(team)];
    }
    throw new IllegalArgumentException();
  }

  public int against(String team1, String team2) {
    if (teams.containsKey(team1) && teams.containsKey(team2)) {
      return againsts[teams.get(team1)][teams.get(team2)];
    }
    throw new IllegalArgumentException();
  }
  public boolean isEliminated(String team) {
    if (!teams.containsKey(team)) throw new IllegalArgumentException();

    int id = teams.get(team);

    if (!termination.containsKey(id)) {
      updateTermination(id);
    }

    return termination.get(id) != null;
  }

  public Iterable<String> certificateOfElimination(String team)  {
    if (!teams.containsKey(team)) throw new IllegalArgumentException();

    int id = teams.get(team);

    if (!termination.containsKey(id)) {
      updateTermination(id);
    }

    return termination.get(id);
  }

  private void updateTermination(int team) {
    LinkedList<String> teamNames = new LinkedList<String>();
    // trivially eliminated
    for (int i = 0; i < num; i++) {
      if (wins[team] + lefts[team] < wins[i]) {
        teamNames.add(names[i]);
      }
    }

    if (!teamNames.isEmpty()) {
      termination.put(team, teamNames);
      return;
    }

    LinkedList<String> ids = BaseballEliminationUtil.buildGraph(team, num,
      wins, lefts, againsts);
    if (ids.isEmpty()) {
      termination.put(team, null);
    } else {
      for (String id : ids) {
        String name = names[Integer.parseInt(id)];
        teamNames.add(name);
      }
      termination.put(team, teamNames);
    }
  }

  public static void main(String[] args) {
    BaseballElimination division = new BaseballElimination(args[0]);
    for (String team : division.teams()) {
        if (division.isEliminated(team)) {
            StdOut.print(team + " is eliminated by the subset R = { ");
            for (String t : division.certificateOfElimination(team)) {
                StdOut.print(t + " ");
            }
            StdOut.println("}");
        } else {
          StdOut.println(team + " is not eliminated");
        }
    }
  }
}

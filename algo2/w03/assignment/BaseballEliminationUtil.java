import java.util.LinkedList;

public class BaseballEliminationUtil {
  public static LinkedList<String> buildGraph(int team, int num,
    int[] wins, int[] lefts, int[][] againsts) {

    String s = "SOURCE";
    String t = "TARGET";

    boolean[] known = new boolean[num];
    FlowNetwork flowNetwork = new FlowNetwork();
    LinkedList<FlowEdge> fromSources = new LinkedList<FlowEdge>();
    for (int i = 0; i < num - 1; i++) {
      if (i == team) continue;
      for (int j = i + 1; j < num; j++) {
        if (j == team) continue;
        String gameName = i + "-" + j;
        FlowEdge fromSource = new FlowEdge(s, gameName, againsts[i][j]);
        fromSources.add(fromSource);
        FlowEdge toTeam1 = new FlowEdge(gameName, i + "", Integer.MAX_VALUE);
        FlowEdge toTeam2 = new FlowEdge(gameName, j + "", Integer.MAX_VALUE);
        flowNetwork.addEdge(fromSource);
        flowNetwork.addEdge(toTeam1);
        flowNetwork.addEdge(toTeam2);
        if (!known[i]) {
          known[i] = true;
          FlowEdge toTarget = new FlowEdge(i + "", t,
            wins[team] + lefts[team] - wins[i]);
          flowNetwork.addEdge(toTarget);
        }
        if (!known[j]) {
          known[j] = true;
          FlowEdge toTarget = new FlowEdge(j + "", t,
            wins[team] + lefts[team] - wins[j]);
          flowNetwork.addEdge(toTarget);
        }
      }
    }

    FF ff = new FF(flowNetwork, s, t);

    return ff.getMinCut();
  }
}

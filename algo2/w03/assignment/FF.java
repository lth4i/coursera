import java.util.LinkedList;
import java.util.HashMap;

public class FF {
  private HashMap<String, Boolean> marked;
  private HashMap<String, FlowEdge> edgeTo;
  private int value;
  private final LinkedList<String> R;

  public FF(FlowNetwork G, String s, String t) {
    value = 0;
    while (hasAugmentingPath(G, s, t)) {
      int bottle = Integer.MAX_VALUE;

      // compute bottleneck capacity
      for (String v = t; !v.equals(s); v = edgeTo.get(v).other(v)) {
        bottle = Math.min(bottle, edgeTo.get(v).residualCapacityTo(v));
      }

      // augment flow
      for (String v = t; !v.equals(s); v = edgeTo.get(v).other(v)) {
        edgeTo.get(v).addResidualFlowTo(v, bottle);
      }

      value += bottle;
    }

    R = new LinkedList<String>();
    for (String e : marked.keySet()) {
      if (e.equals(s) || e.equals(t) || e.contains("-")) continue;
      R.add(e);
    }
  }

  public LinkedList<String> getMinCut() {
    return R;
  }

  private boolean hasAugmentingPath(FlowNetwork G, String s, String t) {
    edgeTo = new HashMap<String, FlowEdge>();
    marked = new HashMap<String, Boolean>();

    LinkedList<String> q = new LinkedList<String>();
    q.add(s);
    marked.put(s, true);

    while (!q.isEmpty()) {
      String v = q.poll();

      for (FlowEdge e : G.adj(v)) {
        String w = e.other(v);

        // find path from s to w in the residual network
        if (e.residualCapacityTo(w) > 0 && !marked.containsKey(w)) {
          edgeTo.put(w, e);
          marked.put(w, true);
          q.add(w);
        }
      }
    }

    return marked.containsKey(t);
  }

  public int value() {
    return value;
  }

  public boolean inCut(int v) {
    return marked.get(v);
  }
}

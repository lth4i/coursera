import java.util.HashMap;
import java.util.ArrayList;

public class FlowNetwork {

  // private final int V;
  private int numV, numE;
  private HashMap<String, ArrayList<FlowEdge>> adj;

  FlowNetwork() {
    this.numV = 0;
    this.numE = 0;
    this.adj = new HashMap<String, ArrayList<FlowEdge>>();
  }

  void addEdge(FlowEdge e) {
    String v = e.from();
    String w = e.to();

    if (!adj.containsKey(v)) {
      adj.put(v, new ArrayList<FlowEdge>());
      numV++;
    }

    if (!adj.containsKey(w)) {
      adj.put(w, new ArrayList<FlowEdge>());
      numV++;
    }

    adj.get(v).add(e);
    adj.get(w).add(e);

    numE++;
  }

  Iterable<FlowEdge> adj(String v) {
    if (adj.containsKey(v))
      return adj.get(v);
    else return new ArrayList<FlowEdge>();
  }

  int numV() {
    return numV;
  }

  int numE() {
    return numE;
  }
}

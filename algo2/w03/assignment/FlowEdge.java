public class FlowEdge {

  private final String v, w;
  private final int capacity;
  private int flow;

  FlowEdge(String v, String w, int capacity) {
    this.v = v;
    this.w = w;
    this.capacity = capacity;
    this.flow = 0;
  }

  String from() {
    return v;
  }

  String to() {
    return w;
  }

  String other(String vertex) {
    if (vertex.equals(this.v)) {
      return w;
    } else {
      return this.v;
    }
  }

  int capacity() {
    return this.capacity;
  }

  int flow() {
    return this.flow;
  }

  int residualCapacity() {
    return capacity - flow;
  }

  int residualCapacityTo(String vertex) {
    if (vertex.equals(this.v)) { // backward
      return flow;
    } else { // forward
      return capacity - flow;
    }
  }

  void addResidualFlowTo(String vertex, int delta) {
    if (vertex.equals(this.v)) {
      flow -= delta;
    } else {
      flow += delta;
    }
  }
}

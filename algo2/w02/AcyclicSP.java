import edu.princeton.cs.algs4.Topological;

public class AcyclicSP extends SP {
  AcyclicSP(EdgeWeightedDigraph g, int s) {
    super(g, s);
  }

  @Override
  void run() {
    Topological t = new Topological(g);
    for (int v : t.order()) {
      for (DirectedEdge e : g.adj(v)) {
        relax(e);
      }
    }
  }
}

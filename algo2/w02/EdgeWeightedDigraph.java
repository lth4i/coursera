import java.util.ArrayList;
import java.util.HashMap;

public class EdgeWeightedDigraph {
  private final int numVertices;
  private int numEdges = 0;
  private final HashMap<Integer, ArrayList<DirectedEdge>> adj;
  private final ArrayList<DirectedEdge> allEdges;

  EdgeWeightedDigraph(int v) {
    this.numVertices = v;
    this.adj = new HashMap<Integer, ArrayList<DirectedEdge>>();
    this.allEdges = new ArrayList<DirectedEdge>();
    for (int i = 0; i < numVertices; i++) {
      adj.put(i, new ArrayList<DirectedEdge>());
    }
  }

  void addEdge(DirectedEdge e) {
    numEdges++;
    adj.get(e.from()).add(e);
    allEdges.add(e);
  }

  Iterable<DirectedEdge> adj(int v) {
    return adj.get(v);
  }

  int v() {
    return numVertices;
  }

  int e() {
    return numEdges;
  }

  Iterable<DirectedEdge> edges() {
    return allEdges;
  }
}

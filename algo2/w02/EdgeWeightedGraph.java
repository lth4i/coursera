import java.util.HashMap;
import java.util.ArrayList;

public class EdgeWeightedGraph {
  private final int v;
  private int e;
  private final HashMap<Integer, ArrayList<Edge>> adjMap;
  private final ArrayList<Edge> edges;

  public EdgeWeightedGraph(int v) {
    this.v = v;
    this.adjMap = new HashMap<Integer, ArrayList<Edge>>();
    this.edges = new ArrayList<Edge>();

    for (int i = 0; i < this.v; i++) {
      this.adjMap.put(i, new ArrayList<Edge>());
    }
  }

  public void addEdge(Edge e) {
    this.e++;
    int v = e.either();
    int w = e.other(v);
    adjMap.get(v).add(e);
    adjMap.get(w).add(e);
    this.edges.add(e);
  }

  public Iterable<Edge> adj(int v) {
    return adjMap.get(v);
  }

  public Iterable<Edge> edges() {
    return edges;
  }

  public int v() {
    return this.v;
  }
}

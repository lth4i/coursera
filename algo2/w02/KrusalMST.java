import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.UF;
import java.util.ArrayList;

public class KrusalMST extends MST {

  public KrusalMST(EdgeWeightedGraph g) {
    super(g);

    MinPQ<Edge> pq = new MinPQ<Edge>();
    for (Edge e : g.edges()) pq.insert(e);

    UF uf = new UF(g.v());
    while(!pq.isEmpty() && mst.size() < g.v() - 1) {
      Edge e = pq.delMin();
      int v = e.either();
      int w = e.other(v);
      if (!uf.connected(v, w)) {
        uf.union(v, w);
        mst.add(e);
      }
    }
  }

}

import edu.princeton.cs.algs4.Picture;
import java.awt.Color;
import edu.princeton.cs.algs4.IndexMinPQ;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Stack;

public class SeamCarver {

  private class Point {
    private final int x, y;
    private Point(int x, int y) {
      this.x = x;
      this.y = y;
    }

    @Override
    public int hashCode() {
      String s = "" + x + y;
      return s.hashCode();
    }
  }

  private final Picture rawPic;
  private final HashMap<Point, Double> energies;

  public SeamCarver(Picture rawPic) {
    if (rawPic == null) throw new java.lang.NullPointerException();
    this.rawPic = rawPic;
    this.energies = new HashMap<Point, Double>();
  }

  private double calculateEnergy(Color c1, Color c2) {
    int red = c1.getRed() - c2.getRed();
    int green = c1.getGreen() - c2.getGreen();
    int blue = c1.getBlue() - c2.getBlue();
    return 0.0 + red * red + green * green + blue * blue;
  }

  public Picture picture() {
    return this.rawPic;
  }

  public int width() {
    return this.rawPic.width();
  }

  public int height() {
    return this.rawPic.height();
  }

  public double energy(int x, int y) {
    return energy(rawPic, x, y);
  }

  private double energy(Picture picture, int x, int y) {
    if (x < 0 || x >= picture.width() || y < 0 || y >= picture.height()) {
      throw new java.lang.IndexOutOfBoundsException(x + "," + y);
    }
    if (x == 0 || x == picture.width() - 1 || y == 0 || y == picture.height() - 1) {
      return 1000.0;
    }

    Color up = picture.get(x, y - 1);
    Color down = picture.get(x, y + 1);
    Color left = picture.get(x - 1, y);
    Color right = picture.get(x + 1, y);
    double xVal = calculateEnergy(left, right);
    double yVal = calculateEnergy(up, down);
    return Math.sqrt(xVal * xVal + yVal * yVal);
  }

  public int[] findHorizontalSeam() {
    return findSeam(this.rawPic);
  }

  public int[] findVerticalSeam() {
    return null;
  }

  private int[] findSeam(Picture picture) {
    double realMinE = Double.MAX_VALUE;
    Point realDest = null;
    HashMap<Point, Point> realPreviousPoint = null;
    for (int x = 1; x <= picture.width() - 2; x++) {
      ArrayList<Point> points = getPoints(picture, x, 1);
      Point root = new Point(x, 1);
      HashMap<Point, Double> eTo = new HashMap<Point, Double>();
      eTo.put(root, energy(x, 1));
      HashMap<Point, Point> previousPoint = new HashMap<Point, Point>();
      previousPoint.put(root, null);
      boolean done = false;
      double minE = Double.MAX_VALUE;
      Point dest = null;
      for (Point from : points) {
        ArrayList<Point> adj = adj(picture, from.x, from.y);
        for (Point to : points) {
          if (!done && to.y == picture.height() - 2) {
            done = true;
          }
          relax(picture, eTo, previousPoint, from, to);
          if (done && eTo.get(to) < minE) {
            minE = eTo.get(to);
            dest = to;
          }
        }
        if (done) break;
      }
      if (dest == null) throw new NullPointerException();
      if (minE < realMinE) {
        realMinE = minE;
        realDest = dest;
        realPreviousPoint = previousPoint;
      }
    }

    int[] seam = new int[picture.height()];
    int counter = picture.height() - 1;
    seam[counter--] = realDest.x;
    for (Point current = realDest; current != null; current = realPreviousPoint.get(current)) {
      seam[counter--] = current.x;
    }
    seam[0] = seam[1];
    return seam;
  }

  public void removeHorizontalSeam(int[] seam) {
    if (seam == null) throw new java.lang.NullPointerException();
  }

  public void removeVerticalSeam(int[] seam) {
    if (seam == null) throw new java.lang.NullPointerException();
  }

  private ArrayList<Point> getPoints(Picture picture, int x, int y) {
    ArrayList<Point> points = new ArrayList<Point>();
    points.add(new Point(x, y));
    int leftMost = x;
    int rightMost = x;
    for (int i = 0; i < picture.height(); i++) {
      if (leftMost > 0) leftMost = leftMost - 1;
      if (rightMost < picture.width() - 1) rightMost = rightMost + 1;
      for (int j = leftMost; j <= rightMost; j++) {
        points.add(new Point(j, i));
      }
    }
    return points;
  }

  private ArrayList<Point> adj(Picture picture, int x, int y) {
    if (y >= picture.height()) throw new java.lang.IndexOutOfBoundsException();
    ArrayList<Point> points = new ArrayList<Point>();
    if (y == picture.height() - 1) return points;
    points.add(new Point(x, y + 1));
    if (x > 0) points.add(new Point(x - 1, y + 1));
    if (x <= picture.width() - 1) points.add(new Point(x + 1, y + 1));
    return points;
  }

  private void relax(Picture picture,
    HashMap<Point, Double> eTo,
    HashMap<Point, Point> previousPoint,
    Point from, Point to) {

    Double toEner = energy(picture, to.x, to.y);
    if (!eTo.containsKey(to) || eTo.get(to) > eTo.get(from) + toEner) {
      eTo.put(to, eTo.get(from) + toEner);
      previousPoint.put(to, from);
    }
  }
}

import java.util.ArrayList;

public class SeamCarverUtil {

  public static int[] findSeam(double[][] grid, int width, int height) {
    if (width == 0 || height == 0) throw new IndexOutOfBoundsException();
    if (width <= 2) {
      int[] seam = new int[height];
      for (int i = 0; i < height; i++) {
        seam[i] = width - 1;
      }
      return seam;
    }
    if (width == 3) {
      int[] seam = new int[height];
      for (int i = 0; i < height; i++) {
        seam[i] = width - 2;
      }
      return seam;
    }
    if (height <= 2) {
      int[] seam = new int[height];
      for (int i = 0; i < height; i++) {
        seam[i] = 0;
      }
      return seam;
    }
    long gStart = System.currentTimeMillis();
    int gDest = 0;
    // HashMap<Point, Point> gPreviousPoint = null;
    int[][] gPreviousPoint = null;
    double gMinE = Double.MAX_VALUE;
    int c = 1;
    for (int x = 1; x <= width - 2; x++) {
      long start = System.currentTimeMillis();
      double[][] eTo = new double[width][height];
      eTo[x][1] = grid[x][1];
      int[][] previousPoint = new int[width][height];

      double minE = Double.MAX_VALUE;
      if (height == 3) minE = grid[x][1];

      int dest = x;
      boolean done = false;

      int leftMost = x;
      int rightMost = x;
      for (int fromY = 1; fromY < height - 1; fromY++) { // each layer
        // if (leftMost == rightMost) {
        //
        // } else if (leftMost == rightMost + 1) {
        //
        // } else {
        //
        // }
        for (int fromX = leftMost; fromX <= rightMost; fromX++) {
          ArrayList<Integer> adj = adj(fromX, fromY, width, height);
          int toY = fromY + 1;
          for (int toX : adj) {
            if (!done && toY == height - 2) {
              done = true;
            }
            relax(grid, eTo, previousPoint,
            fromX, fromY, toX, toY);
            if (done && eTo[toX][toY] < minE) {
              minE = eTo[toX][toY];
              dest = toX;
            }
          }
        }
        if (leftMost > 1) leftMost = leftMost - 1;
        if (rightMost < width - 2) rightMost = rightMost + 1;
      }

      if (minE < gMinE) {
        gMinE = minE;
        gDest = dest;
        gPreviousPoint = previousPoint;
      }
      long end = System.currentTimeMillis();
      System.out.println(c++ + ":\t" + (end - start));
    }
    // System.out.println("Dest = (" + gDest.x + ", " + gDest.y + ")");
    int[] seam = new int[height];
    int counter = height - 1;
    seam[counter--] = gDest;
    for (int x = gDest;
         x != 0;
         x = gPreviousPoint[x][counter + 1]) {
      // System.out.println(counter);
      seam[counter--] = x;
    }
    if (counter != 0) throw new IllegalArgumentException("counter = " + counter);
    seam[0] = seam[1];

    long gEnd = System.currentTimeMillis();
    System.out.println(gEnd - gStart);
    return seam;
  }

  private static ArrayList<Integer> adj(int x, int y, int width, int height) {
    if (y >= height) {
      throw new java.lang.IndexOutOfBoundsException(y + " >= " + height);
    }
    ArrayList<Integer> points = new ArrayList<Integer>();
    if (y == height - 1) return points;
    points.add(x);
    if (x > 1) points.add(x - 1);
    if (x <= width - 2) points.add(x + 1);
    return points;
  }

  private static void relax(double[][] grid,
    double[][] eTo,
    int[][] previousPoint,
    int fromX, int fromY, int toX, int toY) {

    double toEner = grid[toX][toY];
    if (eTo[toX][toY] == 0 ||
      eTo[toX][toY] > eTo[fromX][fromY] + toEner) {
      eTo[toX][toY] = eTo[fromX][fromY] + toEner;
      previousPoint[toX][toY] = fromX;
    }
  }
}

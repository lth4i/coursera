import edu.princeton.cs.algs4.Picture;
import java.awt.Color;

public class SeamCarver {

  private int[][] r, g, b;

  private int w, h;

  public SeamCarver(Picture picture) {
    if (picture == null) throw new java.lang.NullPointerException();
    w = picture.width();
    h = picture.height();
    init(picture);
  }

  private void init(Picture picture) {
    int width = width();
    int height = height();
    r = new int[width][height];
    g = new int[width][height];
    b = new int[width][height];
    for (int x = 0; x < width(); x++) {
      for (int y = 0; y < height(); y++) {
        Color c = picture.get(x, y);
        r[x][y] = c.getRed();
        g[x][y] = c.getGreen();
        b[x][y] = c.getBlue();
      }
    }
  }

  private double[][] calculateEnergies(boolean impose) {
    double[][] energies;
    if (impose) {
      energies = new double[h][w];
    } else {
      energies = new double[w][h];
    }
    for (int x = 0; x < w; x++) {
      for (int y = 0; y < h; y++) {
        if (!impose) {
          energies[x][y] = calculateEnergy(x, y);
        } else {
          energies[y][x] = calculateEnergy(x, y);
        }
      }
    }
    return energies;
  }

  private double calculateEnergy(int x, int y) {
    if (x == 0 || x == w - 1 || y == 0 || y == h - 1) {
      return 1000.0;
    }
    double xVal = calculateEnergy(r[x-1][y], g[x-1][y], b[x-1][y],
    r[x+1][y], g[x+1][y], b[x+1][y]);
    double yVal = calculateEnergy(r[x][y-1], g[x][y-1], b[x][y-1],
    r[x][y+1], g[x][y+1], b[x][y+1]);
    return Math.sqrt(xVal + yVal);
  }

  private double calculateEnergy(int r1, int g1, int b1,
      int r2, int g2, int b2) {
    int red = r1 - r2;
    int green = g1 - g2;
    int blue = b1 - b2;
    return 0.0 + red * red + green * green + blue * blue;
  }

  public Picture picture() {
    Picture p = new Picture(width(), height());
    for (int x = 0; x < width(); x++) {
      for (int y = 0; y < height(); y++) {
        p.set(x, y, new Color(r[x][y], g[x][y], b[x][y]));
      }
    }
    return p;
  }

  public int width() {
    return w;
  }

  public int height() {
    return h;
  }

  public double energy(int x, int y) {
    if (x < 0 || x >= width() || y < 0 || y >= height()) {
      throw new java.lang.IndexOutOfBoundsException(x + "," + y);
    }
    return calculateEnergy(x, y);
  }

  public int[] findHorizontalSeam() {
    double[][] tEnergies = calculateEnergies(true);
    int[] seam = SeamCarverUtil.findSeam(tEnergies, height(), width());
    return seam;
  }

  public int[] findVerticalSeam() {
    double[][] energies = calculateEnergies(false);
    return SeamCarverUtil.findSeam(energies, width(), height());
  }

  public void removeVerticalSeam(int[] seam) {
    if (seam == null) throw new java.lang.NullPointerException();
    if (width() <= 1) throw new java.lang.IllegalArgumentException();
    if (seam.length != height())
      throw new java.lang.IllegalArgumentException();
    checkSeam(seam, width());
    Picture p = new Picture(width() - 1, height());

    int[][] nR = new int[width() - 1][height()];
    int[][] nG = new int[width() - 1][height()];
    int[][] nB = new int[width() - 1][height()];

    for (int y = 0; y < height(); y++) {
      for (int x = 0; x < width() - 1; x++) {
        if (x >= seam[y]) {
          nR[x][y] = r[x + 1][y];
          nG[x][y] = g[x + 1][y];
          nB[x][y] = b[x + 1][y];
        } else {
          nR[x][y] = r[x][y];
          nG[x][y] = g[x][y];
          nB[x][y] = b[x][y];
        }
      }
    }
    r = nR;
    g = nG;
    b = nB;
    w = w - 1;

  }

  public void removeHorizontalSeam(int[] seam) {
    if (seam == null) throw new java.lang.NullPointerException();
    if (height() <= 1) throw new java.lang.IllegalArgumentException();
    if (seam.length != width())
      throw new java.lang.IllegalArgumentException();
    checkSeam(seam, height());

    int[][] nR = new int[width()][height() - 1];
    int[][] nG = new int[width()][height() - 1];
    int[][] nB = new int[width()][height() - 1];

    for (int i = 0; i < width(); i++) {
      nR[i] = removeEntry(r[i], seam[i]);
      nG[i] = removeEntry(g[i], seam[i]);
      nB[i] = removeEntry(b[i], seam[i]);
    }
    r = nR;
    g = nG;
    b = nB;
    this.h = this.h - 1;
  }

  private int[] removeEntry(int[] src, int loc) {
    int[] des = new int[src.length - 1];
    System.arraycopy(src, 0, des, 0, loc);
    System.arraycopy(src, loc + 1, des, loc, src.length - loc - 1);
    return des;
  }

  private void checkSeam(int[] seam, int width) {
    int length = seam.length;
    if (seam[0] < 0 || seam[0] >= width) {
      throw new java.lang.IllegalArgumentException();
    }
    int current = 0;
    for (int i = 1; i < length; i++) {
      if (seam[i] < 0 || seam[i] >= width) {
        throw new java.lang.IllegalArgumentException();
      }
      int diff = seam[current] - seam[i];
      if (diff < -1 || diff > 1) {
        throw new java.lang.IllegalArgumentException();
      }
      current = i;
    }
  }

  public static void main(String[] args) {
    Picture p = new Picture(args[0]);
    SeamCarver sc = new SeamCarver(p);
    for (int i = 0; i < 1; i++) {
      int[] seam = sc.findVerticalSeam();
      sc.removeVerticalSeam(seam);
    }
  }
}

import edu.princeton.cs.algs4.IndexMinPQ;

public class DijkstraSP extends SP {
  private IndexMinPQ<Double> pq;

  DijkstraSP(EdgeWeightedDigraph g, int s) {
    super(g, s);
    pq = new IndexMinPQ<Double>(g.v());
  }

  @Override
  void run() {
    pq.insert(s, 0.0);
    while (!pq.isEmpty()) {
      int v = pq.delMin();
      for (DirectedEdge e : g.adj(v)) {
        relax(e);
      }
    }
  }

  @Override
  protected void relax(DirectedEdge e) {
    int v = e.from();
    int w = e.to();
    if (distTo[w] > distTo[v] + e.weight()) {
      distTo[w] = distTo[v] + e.weight();
      edgeTo[w] = e;
      if (pq.contains(w)) {
        pq.decreaseKey(w, distTo[w]);
      } else {
        pq.insert(w, distTo[w]);
      }
    }
  }
}

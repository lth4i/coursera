import edu.princeton.cs.algs4.MinPQ;
import java.util.ArrayList;

public class LazyPrimMST extends MST {
  private boolean[] marked;
  private MinPQ<Edge> pq;

  public LazyPrimMST(EdgeWeightedGraph g) {
    super(g);
    pq = new MinPQ<Edge>();
    marked = new boolean[g.v()];
    visit(0);

    while(!pq.isEmpty()) {
      Edge e = pq.delMin();
      int v = e.either();
      int w = e.other(v);

      if (marked[v] && marked[w]) continue;
      mst.add(e);
      if (!marked[v]) visit(v);
      if (!marked[w]) visit(w);
    }
  }

  private void visit(int v) {
    marked[v] = true;
    for (Edge e : g.adj(v)) {
      if (!marked[e.other(v)]) {
        pq.insert(e);
      }
    }
  }
}

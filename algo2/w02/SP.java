import java.util.Stack;

public abstract class SP {
  protected final double[] distTo;
  protected final DirectedEdge[] edgeTo;
  protected final EdgeWeightedDigraph g;
  protected final int s;
  SP(EdgeWeightedDigraph g, int s) {
    this.g = g;
    this.s = s;
    distTo = new double[g.v()];
    edgeTo = new DirectedEdge[g.v()];

    for (int v = 0; v < g.v(); v++) {
      distTo[v] = Double.POSITIVE_INFINITY;
    }
    distTo[s] = 0;

    run();
  }

  abstract void run();

  double distTo(int v) {
    return distTo[v];
  }

  Iterable<DirectedEdge> pathTo(int v) {
    Stack<DirectedEdge> path = new Stack<DirectedEdge>();
    for (DirectedEdge e = edgeTo[v]; e != null; e = edgeTo[e.from()]) {
      path.push(e);
    }
    return path;
  }

  protected void relax(DirectedEdge e) {
    int v = e.from();
    int w = e.to();
    if (distTo[w] > distTo[v] + e.weight()) {
      distTo[w] = distTo[v] + e.weight();
      edgeTo[w] = e;
    }
  }

  boolean hasNegativeCycle() {

  }

  Iterable<DirectedEdge> negativeCycles() {

  }
}

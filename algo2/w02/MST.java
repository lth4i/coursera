import java.util.ArrayList;

public abstract class MST {
  protected final EdgeWeightedGraph g;
  protected ArrayList<Edge> mst;
  public MST(EdgeWeightedGraph g) {
    this.g = g;
    mst = new ArrayList<Edge>();
  }

  public Iterable<Edge> edges() {
    return mst;
  }
}

import java.util.LinkedList;

public class TrieST {

  private static final int R = 26;

  private static class Node {
    private Boolean val;
    private Node[] next = new Node[R];
  }

  private Node root = new Node();

  public void put(String k, Boolean v) {
    root = put(root, k, v, 0);
  }

  private Node put(Node x, String k, Boolean v, int i) {
    if (x == null) x = new Node();
    if (i == k.length()) {
      x.val = v;
      return x;
    }
    char c = k.charAt(i);
    int index = Character.getNumericValue(c) - 10;
    // System.out.println(c + ", " + Character.getNumericValue(c));
    x.next[index] = put(x.next[index], k, v, i + 1);
    return x;
  }

  public boolean contains(String k) {
    return get(k) != null;
  }

  public Boolean get(String k) {
    Node n = get(root, k, 0);
    if (n == null) return null;
    return (Boolean) n.val;
  }

  private Node get(Node x, String k, int i) {
    if (x == null) return null;
    if (i == k.length()) return x;
    char c = k.charAt(i);
    int index = Character.getNumericValue(c) - 10;
    return get(x.next[index], k, i + 1);
  }

  public Iterable<String> keys() {
    LinkedList<String> queue = new LinkedList<String>();
    collect(root, "", queue);
    return queue;
  }

  private void collect(Node x, String prefix, LinkedList<String> q) {
    if (x == null) return;
    if (x.val != null) q.add(prefix);
    for (char c = 0; c < R; c++) {
      collect(x.next[c], prefix + c, q);
    }
  }

  public boolean prefixExist(String prefix) {
    return get(root, prefix, 0) != null;
  }

  public Iterable<String> keysWithPrefix(String prefix) {
    LinkedList<String> queue = new LinkedList<String>();
    Node x = get(root, prefix, 0);
    collect(x, prefix, queue);
    return queue;
  }

  // public String longestPrefixOf(String query) {
  //   int length = search(root, query, 0, 0);
  //   return query.subString(0, length);
  // }

  // private int search(Node x, String query, int d, int length) {
  //   if (x == null) return length;
  //   if (x.val != null) length = d;
  //   if (d == query.length()) return length;
  //   char c = query.charAt(d);
  //   return search(x.next[c], query, d + 1, length);
  // }
}

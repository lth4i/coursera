import java.util.HashSet;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

public class BoggleSolver {
  private final TrieST trie;

  private int l = 0;
  // Initializes the data structure using the given array of strings as the dictionary.
  // (You can assume each word in the dictionary contains only the uppercase letters A through Z.)
  public BoggleSolver(String[] dictionary) {
    trie = new TrieST();
    for (int i = 0; i < dictionary.length; i++) {
      if (dictionary[i].length() > l) l = dictionary[i].length();
      trie.put(dictionary[i], true);
    }
  }

  // Returns the set of all valid words in the given Boggle board, as an Iterable.
  public Iterable<String> getAllValidWords(BoggleBoard board) {
    if (board == null) throw new NullPointerException();
    boolean[][] selected = new boolean[board.rows()][board.cols()];
    HashSet<String> words = new HashSet<String>();
    for (int i = 0; i < board.rows(); i++) { // row
      for (int j = 0; j < board.cols(); j++) { // column
        selected[i][j] = true;
        StringBuilder start = new StringBuilder("" + board.getLetter(i, j));
        if (board.getLetter(i, j) == 'Q') start.append("U");
        doSomething(board, i, j, selected,
          start.toString(), words);
        selected[i][j] = false;
      }
    }
    return words;
  }

  private void doSomething(BoggleBoard board,
   int i, int j, boolean[][] selected, String current,
   HashSet<String> words) {

    if (!trie.prefixExist(current)) return;

    for (int x = i - 1; x <= i + 1; x++) {
      for (int y = j - 1; y <= j + 1; y++) {
        if (x == i && y == j) { continue; }
        if (x < 0 || x >= board.rows()) { continue; }
        if (y < 0 || y >= board.cols()) { continue; }
        if (selected[x][y]) { continue; }
        // do something

        // String newCurrent = current + board.getLetter(x, y);
        StringBuilder sb = new StringBuilder(current + board.getLetter(x, y));
        if (board.getLetter(x, y) == 'Q') sb.append("U");
        String newCurrent = sb.toString();
        // System.out.println("\t" + x + "," + y + " " + current);
        selected[x][y] = true;
        if (trie.contains(newCurrent) && newCurrent.length() >= 3) {
          words.add(newCurrent);
        }
        doSomething(board, x, y, selected,
          newCurrent,
          words);

        selected[x][y] = false;

      }
    }
  }

  // Returns the score of the given word if it is in the dictionary, zero otherwise.
  // (You can assume the word contains only the uppercase letters A through Z.)
  public int scoreOf(String word) {
    if (word == null) throw new NullPointerException();
    if (!trie.contains(word)) return 0;
    int length = word.length();
    if (length <= 2) return 0;
    if (length <= 4) return 1;
    if (length == 5) return 2;
    if (length == 6) return 3;
    if (length == 7) return 5;
    return 11;
  }


  public static void main(String[] args)
  {
      In in = new In(args[0]);
      String[] dictionary = in.readAllStrings();
      BoggleSolver solver = new BoggleSolver(dictionary);
      BoggleBoard board = new BoggleBoard(args[1]);
      int score = 0;
      int counter = 1;
      for (String word : solver.getAllValidWords(board))
      {
          StdOut.println(counter++ + "\t" + word);
          score += solver.scoreOf(word);
      }
      StdOut.println("Score = " + score);
  }
}

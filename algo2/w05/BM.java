public class BM {

  // private int[] right;
  private static final int R = 256;
  // private final String pat;
  //
  // public BM(String pat) {
  //   this.pat = pat;
  //
  //   right = new int[256];
  //   for (int c = 0; c < R; c++) right[c] = -1;
  //   for (int j = 0; j < M; j++) right[pat.charAt(j)] = j;
  // }

  public static int search(String txt, String pat) {
    int N = txt.length();
    int M = pat.length();

    int[] right = new int[256];
    for (int c = 0; c < R; c++) right[c] = -1;
    for (int j = 0; j < M; j++) right[pat.charAt(j)] = j;

    int skip;

    for (int i = 0; i <= N - M; i += skip) {
      skip = 0;
      for (int j = M - 1; j >= 0; j--) {
        if (j == M - 1) System.out.print(txt.charAt(i + j) + " ");
        if (pat.charAt(j) != txt.charAt(i + j)) {
          skip = Math.max(1, j - right[txt.charAt(i + j)]);
          break;
        }
      }
      if (skip == 0) return i; // match
    }
    System.out.println();
    return N;
  }

  public static void main(String[] args) {
    String pat = "I S I N T H E".replace(" ", "");
    String txt = "P R I S O N I N T H E N I G H T A N D H E I S I N T H E M I"
      .replace(" ", "");
    search(txt, pat);
  }
}

public class KMP {
  private final int[][] dfa;
  private final int M;
  private final String pat;
  private final int R;

  public KMP(String pat, int R) {
    this.R = R;
    this.pat = pat;
    M = pat.length();

    dfa = new int[R][M];

    dfa[pat.charAt(0)][0] = 1;

    for (int X = 0, j = 1; j < M; j++) {
      for (int c = 0; c < R; c++) {
        dfa[c][j] = dfa[c][X]; // copy mismatch case
      }
      dfa[pat.charAt(j)][j] = j + 1; // set match case
      X = dfa[pat.charAt(j)][X]; // update restart state
    }
  }

  public KMP (String pat) {
    this(pat, 256);
  }

  public int search(String txt) {
    int i, j;
    int N = txt.length();

    for (i = 0, j = 0; i < N && j < M; i++) {
      j = dfa[txt.charAt(i)][j]; // no backup
    }

    if (j == M) return i = M;
    else return N;
  }

  public static void main (String[] args) {
    String pat = "C B C B C A C C".replaceAll(" ", "");
    char[] cs = new char[]{'A', 'B', 'C'};
    KMP kmp = new KMP(pat);
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j <  pat.length(); j++) {
        System.out.print(kmp.dfa[cs[i]][j] + " ");
      }
      System.out.println();
    }
  }
}

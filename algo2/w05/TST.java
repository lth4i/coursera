public class TST<Value> {
  private class Node {
    private Value val;
    private char c;
    private Node left, middle, right;
  }

  private Node root;

  public void put(String key, Value val) {
    root = put(root, key, val, 0);
  }

  private Node put(Node x, String key, Value val, int d) {
    char c = key.charAt(d);
    if (x == null) {
      x = new Node();
      x.c = c;
    }

    if (c < x.c) x.left = put(x.left, key, val, d);
    else if (c > x.c) x.right = put(x.right, key, val, d);
    else if (d < key.length() - 1) x.middle = put(x.middle, key, val, d + 1);
    else x.val = val;

    return x;
  }
}

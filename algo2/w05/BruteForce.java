public class BruteForce {

  public static int alternativeSearch(String pat, String txt) {
    int i, j;
    int N = txt.length();
    int M = pat.length();

    for (i = 0, j = 0; i < N && j < M; i++) {
      if (txt.charAt(i) == pat.charAt(j)) j++;
      else { // backup
        i -= j;
        j = 0;
      }
    }

    if (j == M) return i - M;
    else return N;
  }
}

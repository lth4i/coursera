public class IntervalST<Key extends Comparable<Key>, Value> {
	public IntervalST()

	public void put(Key lo, Key hi, Value val)

	public Value get(Key lo, Key hi)

	public void delete(Key lo, Key hi)

	public Iterable<Value> intersects(Key hi, Key lo) {
		Node x = root;

		while (x != null) {
			if (x.interval.intersects(lo, hi)) return x.interval;
			else if (x.left == null || x.left.hi < lo) x = x.right;
			else x = x.left;
		}

		return x;
	}
}
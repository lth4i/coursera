import java.util.TreeSet;
import java.util.ArrayList;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;

public class PointSET {

    private final TreeSet<Point2D> set = new TreeSet<Point2D>();

    public boolean isEmpty() {
        return set.isEmpty();
    }

    public int size() {
        return set.size();
    }
    public void insert(Point2D p) {
        if (p == null) throw new java.lang.NullPointerException();

        if (!contains(p)) {
            Point2D newPoint = new Point2D(p.x(), p.y());
            set.add(newPoint);
        }
    }

    public boolean contains(Point2D p) {
        if (p == null) throw new java.lang.NullPointerException();

        return set.contains(p);
    }

    public void draw() {
        StdDraw.setPenColor(StdDraw.RED);
        StdDraw.setPenRadius(0.01);
        for (Point2D p : set) {
            p.draw();
        }
    }

    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null) throw new java.lang.NullPointerException();

        ArrayList<Point2D> points = new ArrayList<Point2D>();
        for (Point2D p : set) {
            if (rect.contains(p)) points.add(p);
        }
        return points;
    }

    public Point2D nearest(Point2D p) {
        if (p == null) throw new java.lang.NullPointerException();

        double maxDistance = Double.MAX_VALUE;
        Point2D nearestPoint = null;
        for (Point2D point : set) {
            double distance = point.distanceSquaredTo(p);
            if (nearestPoint == null || maxDistance > distance) {
                maxDistance = distance;
                nearestPoint = point;
            }
        }
        return nearestPoint;
    }

    public static void main(String[] args) {
        int num = 10;

        PointSET ps = new PointSET();
        for (int i = 1; i <= num; i++) {
            Point2D p = new Point2D(i, i);
            ps.insert(p);
        }

        for (int i = 1; i <= num; i++) {
            Point2D p = new Point2D(i, i);
            Point2D nearestPoint = ps.nearest(p);
            System.out.println(String.format("%s\t%s", p, nearestPoint));
        }
    }
}
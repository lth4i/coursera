import java.util.ArrayList;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;

public class KdTree {

    private class Node {
        private final Point2D p;
        private Node left, right;

        private Node(Point2D p) {
            this.p = p;
        }

        public boolean equals(Object obj) {
            if (obj == null) return false;
            if (obj.getClass().equals(Node.class)) {
                Node that = (Node) obj;
                return this.p.equals(that.p);
            }
            return false;
        }
    }

    private Node root = null;
    private int size = 0;

    public boolean isEmpty() { return root == null; }
    public int size() { return size; }

    public void insert(Point2D p) {
        if (p == null) throw new java.lang.NullPointerException();
        Point2D newPoint = new Point2D(p.x(), p.y());
        root = insert(newPoint, root, 1);
    }

    private Node insert(Point2D p, Node n, int level) {
        if (n == null) { // if there is no node
            this.size++;
            return new Node(p);
        }

        if (n.p.equals(p)) { // if existing point is inserted
            return n;
        }

        double nodeVal, pointVal;
        if (level % 2 == 1) {
            // in odd level, x-coordinate is used
            nodeVal = n.p.x();
            pointVal = p.x();
        } else {
            // in even level, y-coordinate is used
            nodeVal = n.p.y();
            pointVal = p.y();
        }

        if (pointVal < nodeVal) {
            n.left = insert(p, n.left, level + 1); // move of the next level on the left
        } else {
            n.right = insert(p, n.right, level + 1); // move to the next level on the right
        }

        return n;
    }

    public boolean contains(Point2D p) {
        if (p == null) throw new java.lang.NullPointerException();
        return contains(p, root, 1); // start searching at root whose level is 1
    }

    private boolean contains(Point2D p, Node n, int level) {
        if (n == null) return false; // if there is no node

        if (n.p.equals(p)) return true; // if node's point equals to given point

        double nodeVal, pointVal;
        if (level % 2 == 1) {
            // in odd level, x-coordinate is used
            nodeVal = n.p.x();
            pointVal = p.x();
        } else {
            // in even level, y-coordinate is used
            nodeVal = n.p.y();
            pointVal = p.y();
        }

        if (pointVal < nodeVal) {
            return contains(p, n.left, level + 1); // move of the next level on the left
        } else {
            return contains(p, n.right, level + 1); // move to the next level on the right
        }
    }

    public void draw() {
        StdDraw.setPenColor(StdDraw.RED);
        StdDraw.setPenRadius(0.01);
        draw(root);
    }

    private void draw(Node n) {
        if (n == null) return;
        draw(n.left);
        n.p.draw();
        draw(n.right);
    }

    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null) throw new java.lang.NullPointerException();

        ArrayList<Point2D> points = new ArrayList<Point2D>();
        range(rect, root, 1, points);

        return points;
    }

    private void range(RectHV rect, Node n, int level, ArrayList<Point2D> points) {
        if (n == null) { // stop if current node is null
            return;
        }

        if (rect.contains(n.p)) { // if current node's point is in an rectangle
            points.add(n.p);
        }
        boolean left = false;
        boolean right = false;
        if (level % 2 == 1) { // vertical line
            if (rect.xmin() < n.p.x()) left = true; // go left if rectangle's left most point is on the left side
            if (rect.xmax() >= n.p.x()) right = true; // go right if rectangle's right most point is on the right side
        } else { // horizontal line
            if (rect.ymin() < n.p.y()) left = true; // go left (i.e. down) if rectangle's bottom point in under
            if (rect.ymax() >= n.p.y()) right = true; // go right (i.e. up) if rectangle's top point in above
        }

        if (left) range(rect, n.left, level + 1, points);
        if (right) range(rect, n.right, level + 1, points);

    }

    public Point2D nearest(Point2D p) {
        if (p == null) throw new java.lang.NullPointerException();

        Node nearestNode = nearest(p, root, null, Double.MAX_VALUE, 1);

        if (nearestNode == null) return null;
        else return nearestNode.p;
    }

    private Node nearest(Point2D p, Node current, Node selected, double minVal, int level) {
        if (current == null) return selected;

        double distance = current.p.distanceSquaredTo(p);
        if (selected == null || distance < minVal) { // update new nearest point if necessary
            minVal = distance;
            selected = current;
        }

        Node firstHalf, secondHalf;
        if (level % 2 == 1) { // vertical line
            if (p.x() < current.p.x()) { // given point is on the left of current node's point
                firstHalf = current.left;
                secondHalf = current.right;
            } else { // given point is on the right of current node's point
                firstHalf = current.right;
                secondHalf = current.left;
            }
        } else { // horizontal line
            if (p.y() < current.p.y()) { // given point is on the bottom of current node's point
                firstHalf = current.left;
                secondHalf = current.right;
            } else { // given point is on the top of current node's point
                firstHalf = current.right;
                secondHalf = current.left;
            }
        }

        Node newSelected = nearest(p, firstHalf, selected, minVal, level + 1); // search the first half
        if (!selected.equals(newSelected)) {
            // don't need to search the second half if the new selected is found on the first half
            return newSelected;
        } else {
            // search the second half if the new selected is not found on the first half
            return nearest(p, secondHalf, selected, minVal, level + 1);
        }
    }

    public static void main(String[] args) {
        int num = 7;
        KdTree t = new KdTree();

        for (int i = 0; i <= num; i++) {
            Point2D p = new Point2D(i, i);
            t.insert(p);
        }
        RectHV r = new RectHV(2.5, 2.5, 4.5, 5.5);
        Iterable<Point2D> points = t.range(r);
        for (Point2D p : points) {
            System.out.println(p);
        }

        Point2D p1 = new Point2D(2, 4);
        Point2D nearest = t.nearest(p1);
        System.out.println(nearest);
    }
}
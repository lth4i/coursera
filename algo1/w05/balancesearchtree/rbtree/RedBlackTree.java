public class RedBlackTree<Key extends Comparable<Key>, Val> {

	private class Node {
		Key k;
		Val v;
		Node left, right;
		boolean isRed; // color of parent link

		private Node(Key k, Val v, boolean isRed) {
			this.k = k;
			this.v = v;
			this.isRed = isRed;
		}
	}

	private Node root;

	private boolean isRed(Node n) {
		if (n == null) return false; // null link is black
		return n.isRed;
	}

	public Val get(Key k) {
		Node x = root;
		while (x != null) {
			int cmp = k.compareTo(x.k);
			if (cmp > 0) x = x.right;
			else if (cmp < 0) x = x.left;
			else return x.v;
		}
		return null;
	}

	private Node rotateLeft(Node n) {
		Node x = n.right;
		n.right = x.left; // make x's left branch to be n's right branch
		x.left = n; // make n to be x's left branch
		x.isRed = n.isRed; // give n's color to x
		n.isRed = true; // n is not connected by a red branch from x
		return x;
	}

	private Node rotateRight(Node n) {
		Node x = n.left;
		n.left = x.right; // make x's right branch to be n's left branch
		x.right = n; // make n to be x's right branch
		x.isRed = n.isRed; // give n's color to x
		n.isRed = true; // n is not connected by a red branch from x
		return x;
	}

	private Node flipColors(Node n) {
		n.isRed = true;
		n.left.isRed = false;
		n.right.isRed = false;
		return n;
	}

	public void put(Key k, Val v) {
		root = put(root, k, v);
	}

	private Node put(Node h, Key k, Val v) {
		if (h == null) return new Node(k, v, true); // insert at bottom

		int cmp = k.compareTo(h.k);
		if (cmp < 0) h.left = put(h.left, k, v);
		else if (cmp > 0) h.right = put(h.right, k, v);
		else h.v = v;

		if (isRed(h.right) && !isRed(h.left)) h = rotateLeft(h); // lean left
		if (isRed(h.left) && isRed(h.left.left)) h = rotateRight(h); // balance 4-node
		if (isRed(h.left) && isRed(h.right)) h = flipColors(h); // split 4-node

		return h;
	}
}
import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;
import java.util.Comparator;
import java.util.List;
import java.util.Arrays;
import java.util.HashSet;

public class Solver {

    private class Node {
        private final Board board;
        private final int num;
        private final Node previousNode;

        private Node(Board board, int num, Node previousNode) {
            this.board = board;
            this.num = num;
            this.previousNode = previousNode;
        }
    }

    private class ManhattanOrder implements Comparator<Node> {
        public int compare(Node n1, Node n2) {
            // calculate priorities of two nodes
            int p1 = n1.board.manhattan() + n1.num;
            int p2 = n2.board.manhattan() + n2.num;

            if (p1 > p2) return 1;
            if (p1 < p2) return -1;
            return 0;
        }
    }

    private HashSet<String> knownBoards;

    private boolean isSolvable = false;
    private int moves = -1;
    private Node goalNode = null;

    public Solver(Board initial) {
        if (initial == null) throw new java.lang.NullPointerException();

        knownBoards = new HashSet<String>();
        knownBoards.add(initial.toString());

        MinPQ<Node> q1 = new MinPQ<Node>(new ManhattanOrder());
        q1.insert(new Node(initial, 0, null));
        MinPQ<Node> q2 = new MinPQ<Node>(new ManhattanOrder()); // twin queue
        q2.insert(new Node(initial.twin(), 0, null));

        int counter = 0;
        while (true) {
            // switch between a main queue (with the real inital board)
            // and a twin board (with the twin inital board)
            MinPQ<Node> q;
            if (counter % 2 == 0) q = q1;
            else q = q2;

            Node node = q.delMin();

            if (node.board.isGoal()) {
                if (counter % 2 == 0) {
                    isSolvable = true;
                    moves = node.num;
                    goalNode = node;
                    // System.out.println("Solvable");
                } else {
                    // System.out.println("Insolvable");
                }
                break;
            } else {
                Iterable<Board> neighbors = node.board.neighbors();
                for (Board neighbor : neighbors) {
                    String nString = neighbor.toString();
                    if (!knownBoards.contains(nString)) { // if the board is never seen before
                        knownBoards.add(nString);
                        q.insert(new Node(neighbor, node.num + 1, node));
                    }
                }
            }
            counter++;
        }
    }

    public boolean isSolvable() { return isSolvable; }
    
    public int moves() { return moves; }
    
    public Iterable<Board> solution() {
        if (!isSolvable) {
            return null;
        }

        int i = moves + 1;
        Board[] boards = new Board[i];
        Node curNode = goalNode;
        while (curNode != null) {
            // System.out.println(curNode.num);
            boards[--i] = curNode.board;
            curNode = curNode.previousNode;
        }
        return (List<Board>) Arrays.asList(boards);
    }

    public static void main(String[] args) {

        // create initial board from file
        In in = new In(args[0]);
        int N = in.readInt();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = in.readInt();
        Board initial = new Board(blocks);

        // solve the puzzle
        Solver solver = new Solver(initial);

        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution())
                StdOut.println(board);
        }
    }

}

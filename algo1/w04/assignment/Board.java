import java.util.ArrayList;
import edu.princeton.cs.algs4.In;

public class Board {

    private final int[][] blocks;

    private final int length;

    private int hamming, manhattan;

    private int emptyX = -1;
    private int emptyY = -1;

    public Board(int[][] givenBlock) {
        if (givenBlock == null) throw new java.lang.NullPointerException();

        length = givenBlock.length;

        blocks = new int[length][length];

        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                int actualValue = givenBlock[i][j];
                blocks[i][j] = actualValue;
                if (actualValue != 0) { // only calculate distance for non-empty cell
                    int expectedValue = expectedValue(i, j);
                    if (expectedValue != actualValue) hamming++;

                    int expectedX = (actualValue - 1) / length;
                    int expectedY = actualValue - expectedX * length - 1;
                    int tmp = Math.abs(expectedX - i) + Math.abs(expectedY - j);
                    manhattan += tmp;
                } else {
                    emptyX = i;
                    emptyY = j;
                }
            }
        }
    }

    private int expectedValue(int x, int y) {
        if (x == length - 1 && y == length - 1) return 0;
        return y + x * length + 1;
    }

    public int dimension() {
        return length;
    }

    public int hamming() { return hamming; }
    public int manhattan() { return manhattan; }
    
    public boolean isGoal() {
        return hamming == 0;
    }
    public Board twin() {
        int x1 = -1;
        int x2 = -1;
        int y1 = -1;
        int y2 = -1;
        int[][] twinBlocks = new int[length][length];

        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                twinBlocks[i][j] = blocks[i][j];
                if (twinBlocks[i][j] == 1) {
                    x1 = i;
                    y1 = j;
                } else if (twinBlocks[i][j] == 2) {
                    x2 = i;
                    y2 = j;
                }
            }
        }

        // swap position of nodes containing 1 and 2
        twinBlocks[x1][y1] = 2;
        twinBlocks[x2][y2] = 1;

        return new Board(twinBlocks);
    }

    private int[][] getArray() {
        int[][] a = new int[length][length];

        // copy value from map to array
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                a[i][j] = blocks[i][j];
            }
        }

        return a;
    }

    public boolean equals(Object y) {
        if (y == null) return false;
        if (y instanceof Board) {
            Board that = (Board) y;
            if (this.dimension() != that.dimension() || this.hamming != that.hamming || this.manhattan != that.manhattan) {
                return false;
            }

            // compare each cell
            for (int i = 0; i < length; i++) {
                for (int j = 0; j < length; j++) {
                    if (this.blocks[i][j] != that.blocks[i][j]) return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public Iterable<Board> neighbors() {
        ArrayList<Board> neighbors = new ArrayList<Board>();

        int x = emptyX;
        int y = emptyY;

        if (x - 1 >= 0) { // swap empty cell with the one on its left
            neighbors.add(new Board(swap(x, y, x - 1, y)));
        }
        if (x + 1 < length) { // swap empty cell with the one on its right
            neighbors.add(new Board(swap(x, y, x + 1, y)));
        }
        if (y - 1 >= 0) { // swap empty cell with the one above it
            neighbors.add(new Board(swap(x, y, x, y - 1)));
        }
        if (y + 1 < length) { // swap empty cell with the one below it
            neighbors.add(new Board(swap(x, y, x, y + 1)));
        }

        return neighbors;
    }

    private int[][] swap(int x1, int y1, int x2, int y2) {
        int[][] a = getArray();
        int tmp = a[x1][y1];
        a[x1][y1] = a[x2][y2];
        a[x2][y2] = tmp;
        return a;

    }

    public String toString() {
        int[][] a = getArray();
        String s = length + "\n";
        for (int i = 0; i < length; i++) {
            s += " ";
            for (int j = 0; j < length; j++) {
                String v = Integer.toString(a[i][j]);
                if (v.length() == 1) v = " " + v;
                s += v + " ";
            }
            s = s.trim();
            s += "\n";
        }
        return s;
    }

    public static void main(String[] args) {
        In in = new In(args[0]);
        int N = in.readInt();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = in.readInt();
        Board b = new Board(blocks);

        System.out.println(b);
        System.out.println(b.twin());

        System.out.println(b.hamming());
        System.out.println(b.manhattan());

        Iterable<Board> neighbors = b.neighbors();
        for (Board n : neighbors) {
            System.out.println();
            System.out.println(n);
        }
    }
}
public abstract class ST<Key extends Comparable<Key>, Value> {
	
	public void put(Key key, Value value)  // NOTE: renove key from table if the key is null

	public Value get(Key key) 

	public void delete(Key key) {
		// lazy implementation
		put(key, null);
	}

	public boolean contains(Key key) {
		return get(key) != null;
	}

	public boolean isEmpty() 

	public int size() 

	public Key min()

	public Key max()

	public Key floor(Key key)

	public Key ceiling(Key key)

	public int rank(Key key)

	public Key select(int k)

	public void deleteMin()

	public void deleteMax()

	public int size(Key lo, Key hi)

	public Iterable<Key> keys(Key lo, Key hi)

	public Iterable<Key> keys()
}
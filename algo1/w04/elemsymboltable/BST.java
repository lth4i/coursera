import java.util.ArrayList;

public class BST<Key extends Comparable<Key>, Value> {

	private class Node {
		private Key k;
		private Value v;
		private Node left;
		private Node right;
		private int count; // number of the node children and itself

		private Node(Key k, Value v) {
			this.k = k;
			this.v = v;
		}
	}

	private Node root;

	public void put(Key k, Value v) {
		root = put(root, k, v);
	}

	private Node put(Node x, Key k, Value v) {
		if (x == null) return new Node(k, v); // if current node is null, create a new one

		int cmp = k.compareTo(x.k);

		if (cmp < 0) x.left = put(x.left, k, v); // update link on the left
		else if (cmp > 0) x.right = put(x.right, k, v); // update link on the right
		else x.v = v; // update value of the current node

		x.count = 1 + size(x.left) + size(x.right); // update the node's count

		return x;
	}

	public Value get(Key k) {
		Node current = root;

		while (current != null) {
			int cmp = k.compareTo(current.k);
			if (cmp > 0) current = current.right; // given key is larger than a current one, go right
			else if (cmp < 0) current = current.left; // given key is less than a current one, go left
			else return current.v; // otherwise, i.e. equal, return the current
		}

		return null; // key not found
	}

	public Key max() {
		Node current = root;

		while (current != null) {
			if (current.right != null) current = current.right; // continue to the right if possible
			else return current.k; // otherwise, return the current node's key
		}
		return null;
	}

	public Key min() {
		Node current = root;

		while (current != null) {
			if (current.left != null) current = current.left; // continue to the left if possible
			else return current.k; // otherwise, return the current node's key
		}
		return null;
	}

	public Key floor(Key k) { // return largest key that is less than or equal to the given key
		Node n = floor(root, k);
		if (n == null) return null;
		else return n.k;
	}

	private Node floor(Node n, Key k) {
		if (n == null) return null;

		int cmp = k.compareTo(n.k);

		if (cmp == 0) return n;

		if (cmp < 0) return floor(n.left, k);

		Node t = floor(n.right, k);
		if (t != null) return t;
		else return n;
	}

	public int size() {
		return size(root);
	}

	private int size(Node n) {
		if (n == null) return 0;
		else return n.count;
	}

	public int rank(Key k) {
		return rank(root, k);
	}

	private int rank(Node n, Key k) {
		if (n == null) return 0;

		int cmp = k.compareTo(n.k);

		// if given key is less than current one's, go to the left 
		if (cmp < 0) return rank(n.left, k);
		
		// if given key is greater than current one's, its rank is the sum of 1 (current node),
		// size of left branch (whose nodes are less than a given key) and rank of the right branch
		if (cmp > 0) return 1 + size(n.left) + rank(n.right, k);
		
		// otherwise, i.e. current key is equal to the current ones, the rank is the size of
		// the left brach, whose nodes are less than the current one, which is equal to the given one
		return size(n.left);
	}

	public Iterable<Key> keys() {
		ArrayList<Key> a = new ArrayList<Key>();
		inorder(root, a);
		return a;
	}

	private void inorder(Node n, ArrayList a) {
		if (n == null) return;
		inorder(n.left, a);
		a.add(n.k);
		inorder(n.right, a);
	}

	public Iterable<Key> levelOrder() {
		ArrayList<Key> a = new ArrayList<Key>();
		levelOrder(root, a);
		return a;
	}

	private void levelOrder(Node n, ArrayList a) {
		if (n == null) return;
		a.add(n.k);
		inorder(n.left, a);
		inorder(n.right, a);
	}

	public void deleteMin() {
		root = deleteMin(root);
	}

	private Node deleteMin(Node n) {
		if (n.left == null) return n.right;
		n.left = deleteMin(n.left);
		n.count = 1 + size(n.left) + size(n.right);
		return n;
	}

	public void delete(Key k) {
		root = delete(root, k);
	}

	private Node delete(Node n, Key k) {
		if (n == null) return null;

		int cmp = k.compareTo(n.k);

		if (cmp < 0) n.left = delete(n.left, k);
		else if (cmp > 0) n.right = delete(n.right, k);
		else { // found the node to delete
			if (n.right == null) return n.left; // if there is no left, replace current node with the right node
			if (n.left == null) return n.right; // if there is no right, replace current node with the left node

			Node t = n;
			n = min(t.right); // replace with successor
			n.right = deleteMin(t.right); // delete successor
			n.left = t.left;
		}
		n.count = 1 + size(n.left) + size(n.right);
		return n;
	}

	private Node min(Node n) {
		if (n == null) throw new NullPointerException();
		if (n.left == null) return n;
		return min(n.left);
	}

	public static void main(String[] args) {
		BST<Integer, Integer> t = new BST<Integer, Integer>();

		String[] items = "94 79 54 30 64 14 48 19 42 53 26 37".split(" ");

		for (int i = 0; i < items.length;  i++) {
			int v = Integer.parseInt(items[i]);
			t.put(v, v);
		}

		System.out.println(t.levelOrder());
	}
}
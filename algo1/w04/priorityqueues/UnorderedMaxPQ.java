public class UnorderedMaxPQ<Key extends Comparable<Key>> {

	private Key[] pq;
	private int size;

	public UnorderedMaxPQ(int capacity) {
		pq = (Key[]) new Comparable[capacity];
		size = 0;
	}

	public void insert(Key v) { pq[size++] = v; }

	public Key delMax() {
		int max = 0;
		for(int i = 1; i < size; i++) {
			if (pq[max].compareTo(pq[i]) < 0) {
				max = i;
			}
		}
		swap(max, size - 1);
		Key result = pq[size - 1];
		pq[--size] = null;
		return result;
	}

	private void swap(int i, int j) {
		Key temp = pq[i];
		pq[i] = pq[j];
		pq[j] = temp;
	}

	public boolean isEmpty() { return size == 0; }

	public static void main(String[] args) {
		int num = 100;
		UnorderedMaxPQ<Integer> q = new UnorderedMaxPQ<Integer>(num);

		for (int i = 0; i < num; i++) {
			q.insert(i);
		}

		while (!q.isEmpty()) {
			System.out.println(q.delMax());
		}
	}
}
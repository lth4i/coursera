public class BinaryHeap<Key extends Comparable<Key>> {

	private Key[] a;
	private int size;

	public BinaryHeap(int capacity) {
		a = (Key[]) new Comparable[capacity];
		size = 0;
	}

	public void insert(Key v) {
		a[size++] = v;
		swim(size - 1);
	}

	public Key delMax() {
		Key result = a[0];
		a[0] = a[size - 1];
		a[--size] = null;
		sink(0);
		return result;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	private void swim(int k) {
		while (k > 0 && less(getParent(k), k)) { // while root is not reached or parent is less than child
			swap(k, getParent(k));
			k = getParent(k);
		}
	}

	private void sink(int k) {
		while (getLeftChild(k) < size) {
			int j = getLeftChild(k);
			if (j < size - 1 && less(j, j + 1)) j++; // compare left child with right child
			if (less(j, k)) break; // compare child with parent, break if parent is greater
			swap(k, j);
			k = j;
		}
	}

	private void sunk(int k) {
		int lChild = getLeftChild(k);
		int rChild = getRightChild(k);

		int selectedChild = -1;

		if (lChild >= size && rChild >= size) {
			selectedChild = -1;
		} else if (lChild >= size) {
			selectedChild = rChild;
		} else if (rChild >= size) {
			selectedChild = lChild;
		} else if (less(lChild, rChild)) {
			selectedChild = rChild;
		} else {
			selectedChild = lChild;
		}

		if (selectedChild != -1 && less(k, selectedChild)) {
			swap(selectedChild, k);
			k = selectedChild;
			sunk(k);
		} else {
			return;
		}
	}

	private int getParent(int i) {
		return (i - 1) / 2;
	}

	private int getLeftChild(int i) {
		return i * 2 + 1;
	}

	private int getRightChild(int i) {
		return i * 2 + 2;
	}

	private boolean less(int n1, int n2) {
		if (a[n1].compareTo(a[n2]) < 0) {
			return true;
		} else {
			return false;
		}
	}

	private void swap(int i, int j) {
		Key temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}

	public String toString() {
		String s = "";
		for (int i = 0; i < size; i++) {
			s += a[i] + " ";
		}
		return s.trim();
	}

	public static void main(String[] args) {
		String[] inputs = "99 73 89 51 46 43 67 16 33 11".split(" ");
		BinaryHeap<Integer> q = new BinaryHeap<Integer>(inputs.length);

		for (int i = 0; i < inputs.length; i++) {
			q.insert(Integer.parseInt(inputs[i]));
			// System.out.println(q.toString());
		}

		System.out.println(q);

		while (!q.isEmpty()) {
			System.out.println(q.delMax());
			System.out.println(q.toString());
		}
	}
}
public class OrderedMaxPQ<Key extends Comparable<Key>> {

	private class Node {
		private final Key v;

		private Node previous;
		private Node next;

		private Node(Key v) { this.v = v; }
	}

	private Node first;

	public OrderedMaxPQ() {
		first = null;
	}

	public void insert(Key v) {
		Node newNode = new Node(v);

		if (first == null) {
			// if there is no node
			first = newNode;
		} else if (less(first, newNode)) {
			// if new node is greater than first node
			first.previous = newNode;
			newNode.next = first;
			first = newNode;
		} else {
			Node previous = first;
			Node next = previous.next;

			while (next != null && less(previous, newNode)) {
				previous = next;
				next = previous.next;
			}

			previous.next = newNode;
			newNode.next = next;
			if (next != null) {
				// the new node NOT is inserted at the end of the queue
				next.previous = newNode;
			}
		}
	}

	private boolean less(Node n1, Node n2) {
		if (n1.v.compareTo(n2.v) < 0) {
			return true;
		} else {
			return false;
		}
	}

	public Key delMax() {
		Node newFirst = first.next;
		if (newFirst != null) {
			newFirst.previous = null;
		}
		Node result = first;
		first = newFirst;

		return result.v;
	}

	public boolean isEmpty() { return first == null; }

	public static void main(String[] args) {
		int num = 100;
		OrderedMaxPQ<Integer> q = new OrderedMaxPQ<Integer>();

		for (int i = 0; i < num; i++) {
			q.insert(i);
		}

		while (!q.isEmpty()) {
			System.out.println(q.delMax());
		}
	}
}
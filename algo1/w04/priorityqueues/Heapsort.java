import java.util.Arrays;

public class Heapsort {

	public static void heapsort(Comparable[] a) {
		int size = a.length;

		// first pass: create a heap
		for (int k = (size - 1) / 2; k >= 0; k--) {
			sink(a, k, size);
		}

		// second pass: remove the greatest item
		while (size > 0) {
			swap(a, 0, --size);
			sink(a, 0, size);
		}
	}

	private static void sink(Comparable[] a, int k, int size) {
		while (k * 2 + 1 < size) {
			int j = k * 2 + 1;
			if (j < size - 1 && less(a, j, j + 1)) j++; // compare left child with right child
			if (less(a, j, k)) break; // compare child with parent, break if parent is greater
			swap(a, k, j);
			k = j;
		}
	}

	private static void swap(Comparable[] a, int i, int j) {
		Comparable temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}

	private static boolean less(Comparable[] a, int n1, int n2) {
		return a[n1].compareTo(a[n2]) < 0;
	}

	public static void main (String[] args) {
		int num = 10;
		Integer[] a = new Integer[num];
		for (int i = 0; i < num; i++) {
			a[i] = num - i;
		}
		System.out.println(Arrays.toString(a));
		Heapsort.heapsort(a);
		System.out.println(Arrays.toString(a));
	}
}
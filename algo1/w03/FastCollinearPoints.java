import java.util.Arrays;
import java.util.ArrayList;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.In;

public class FastCollinearPoints {

    private final LineSegment[] lines;
    private int numLines = 0;
    private final Point[] points;

    public FastCollinearPoints(Point[] parPoints) {
        if (parPoints == null) {
            throw new java.lang.NullPointerException();
        }

        points = new Point[parPoints.length];
        for (int i = 0; i < parPoints.length; i++) {
            Point p = parPoints[i];
            points[i] = p;
        }

        LineSegment[] _lines = new LineSegment[points.length];

        for (int c1 = 0; c1 < points.length; c1++) {
            Point p = points[c1];

            if (p == null) throw new java.lang.NullPointerException();

            // System.out.println("Checking " + p);

            Point[] _points = new Point[points.length];
            System.arraycopy(points, 0, _points, 0, points.length);

            Arrays.sort(_points, p.slopeOrder());
            int i = 0;
            int j = 1;
            int numDuplicate = 0;
            // ArrayList<ArrayList<Point>> listOfPoints = new ArrayList<ArrayList<Point>>();
            ArrayList<Point> __points = new ArrayList<Point>();

            // System.out.println(p);
            // for (Point _p : _points) System.out.println("\t" + _p + "\t" + p.slopeTo(_p));
            // System.out.println();

            while (j < _points.length) {
                Point p1 = _points[i];
                Point p2 = _points[j];

                // System.out.println(String.format("%s\t%s\t%s\t%s\t%s", p, p1, p2, p.compareTo(p1), p.compareTo(p2)));

                // only duplicate is allow
                if (p.compareTo(p1) == 0) {
                    if (numDuplicate < 2) {
                        numDuplicate++;
                        i++;
                        if (i == j) j++;
                        continue;
                    }
                    else throw new java.lang.IllegalArgumentException();
                }
                if (p.compareTo(p2) == 0) {
                    if (numDuplicate < 2) {
                        numDuplicate++;
                        j++;
                        continue;
                    }
                    else throw new java.lang.IllegalArgumentException();
                }

                double sl1 = p.slopeTo(p1);
                double sl2 = p.slopeTo(p2);

                // if (sl1 == sl2) System.out.println(String.format("%s\t%s\t%s\t%s\t%s\t%s", p, p1, p2, sl1, sl2, sl1 == sl2));

                if (sl1 == sl2) { // if two points have the same slope with the current selected point
                    // System.out.println(String.format("%s\t%s\t%s\t%s\t%s\t%s", p, p1, p2, sl1, sl2, sl1 == sl2));
                    if (__points.isEmpty()) {
                        // listOfPoints.add(__points);
                        __points.add(p1);
                    }
                    __points.add(p2);
                    j++;
                } else if (__points.isEmpty()) {
                    i++;
                    j++;
                } else {
                    // System.out.println(String.format("%s\t%s\t%s\t%s\t%s\t%s", p, p1, p2, sl1, sl2, sl1 == sl2));
                    if (__points.size() >= 3) {
                        Point lp = p; // left-most point
                        Point rp = null; // right-most point
                        for (Point __p : __points) {
                            if (lp.compareTo(__p) > 0) {
                                // if current point is not the left most one, do not create a line
                                // wait for another point which is the left most one of this line
                                lp = null;
                                break;
                            }
                            if (rp == null || rp.compareTo(__p) < 0) {
                                rp = __p;
                            }
                        }
                        if (lp != null) {
                            LineSegment l = new LineSegment(lp, rp);
                            _lines[numLines++] = l;
                            if (numLines == _lines.length) {
                                LineSegment[] temp = new LineSegment[numLines * 2];
                                for (int _c = 0; _c < numLines; _c++) {
                                    temp[_c] = _lines[_c];
                                }
                                _lines = temp;
                            }
                        }
                    }
                    __points = new ArrayList<Point>();
                    i = j;
                    j = i + 1;
                }
            }
            if (__points.size() >= 3) {
                Point lp = p; // left-most point
                Point rp = null; // right-most point
                for (Point __p : __points) {
                    if (lp.compareTo(__p) > 0) {
                        // if current point is not the left most one, do not create a line
                        // wait for another point which is the left most one of this line
                        lp = null;
                        break;
                    }
                    if (rp == null || rp.compareTo(__p) < 0) {
                        rp = __p;
                    }
                }
                if (lp != null) {
                    LineSegment l = new LineSegment(lp, rp);
                    _lines[numLines++] = l;
                }
            }
        }

        // System.out.println("Add " + numLines + " lines");

        lines = new LineSegment[numLines];
        System.arraycopy(_lines, 0, lines, 0, numLines);

    }

    public           int numberOfSegments() { return numLines; }
    public LineSegment[] segments() { return lines; }

    public static void main(String[] args) {
        // read the N points from a file
        In in = new In(args[0]);
        int N = in.readInt();
        Point[] points = new Point[N];
        for (int i = 0; i < N; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }

        // draw the points
        StdDraw.show(0);
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        FastCollinearPoints collinear = new FastCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
    }
}
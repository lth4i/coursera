
public class Mergesort {

	private static int counter = 0;

	private static void merge(Comparable[] a, Comparable[] aux, final int lo, final int mid, final int hi) {
		// assert isSorted(a, lo, mid);		// precondition: a[lo..mid]		sorted
		// assert isSorted(a, mid + 1, hi);	// precondition: a[mid+1..hi]	sorted

		// copy all items from the main array to the auxilary one
		for (int k = lo; k <= hi; k++) {
			aux[k] = a[k];
		}

		int i = lo;
		int j = mid + 1;

		for (int k = lo; k <= hi; k++) {
			if (i > mid) {
				// all the first half is added, keep adding the remaining second half
				a[k] = aux[j++];
			} else if (j > hi) {
				// all the second half is added, keep adding the remaining first half
				a[k] = aux[i++];
			} else if (aux[i].compareTo(aux[j]) > 0) {
			// } else if (aux[i] > aux[j]) {
				// if an item in the first half  is greater than another item in the second half
				// put an item in the second half in
				a[k] = aux[j++];
			} else {
				// otherwise, i.e. an item in the first half is not greater than another one on the second half
				// put an item in the first half in
				a[k] = aux[i++];
			}
		}

		String s = getString(a);
		System.out.println(String.format("%s\t%s",++counter, s));
		// assert isSorted(a, lo, hi);
	}

	private static void sort(Comparable[] a, Comparable[] aux, int lo, int hi) {
		if (lo >= hi) {
			return;
		}

		// if (lo + CUTOFF - 1>= hi) {
		// 	Insertion.sort(a, lo, hi);
		// 	return;
		// }

		int mid = (lo + hi) / 2;
		sort(a, aux, lo, mid); // sort first half
		sort(a, aux, mid + 1, hi); // sort second half

		// if (a[mid].compareTo(a[mid + 1]) <= 0) {
			// if the last, i.e. greatest, item of the first half is less than or equal to
			// the first, i.e. least, item of the second half, the two halves are already sorted
		// 	return;
		// }

		merge(a, aux, lo, mid, hi); // merge
	}

	private static void sort(Comparable[] a) {
		// create auxilary array outside of the recursive process
		// in order to prevent additional resources, otherwise
		// each recursive call will create one auxilary array
		Comparable[] aux = new Comparable[a.length];
		sort(a, aux, 0, a.length - 1);
	}

	private static void bottomupSort(Comparable[] a) {
		final int SIZE = a.length;
		Comparable[] aux = new Comparable[a.length];

		for (int sz = 1; sz < SIZE; sz = sz * 2) {
			for (int lo = 0; lo < SIZE - sz; lo += sz * 2) {
				// System.out.println(sz);
				merge(a, aux, lo, lo + sz - 1, Math.min(lo + sz * 2 - 1, SIZE - 1));
			}
		}
	}

	private static String getString(Comparable[] a) {
		String s = "";
		for (int i = 0; i < a.length; i++) {
			s = s + " " + a[i];
		}
		return s.trim();
	}

	public static void main (String[] args) {
		String s1 = "94 12 98 82 16 32 79 11 47 77 81 44";
		String[] a1 = s1.split(" ");

		sort(a1);

		counter = 0;

		String s2 = "61 13 76 60 69 59 58 10 27 89";
		String[] a2 = s2.split(" ");
		bottomupSort(a2);
	}
}
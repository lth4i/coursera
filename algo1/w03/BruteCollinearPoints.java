import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.In;

public class BruteCollinearPoints {

    private final LineSegment[] segments;
    private final Point[] points;

    public BruteCollinearPoints(Point[] parPoints) {
        if (parPoints == null) {
            throw new java.lang.NullPointerException();
        }

        points = new Point[parPoints.length];
        for (int i = 0; i < parPoints.length; i++) {
            Point p = parPoints[i];
            points[i] = p;
        }

        LineSegment[] _segments = new LineSegment[points.length];
        int numLines = 0;

        int numPoints = points.length;
        for (int c1 = 0; c1 < numPoints; c1++) {
            Point p1 = points[c1];
            if (p1 == null) {
                throw new java.lang.NullPointerException();
            }
            for (int c2 = c1 + 1; c2 < numPoints; c2++) {
                Point p2 = points[c2];
                if (p2 == null) {
                    throw new java.lang.NullPointerException();
                }
                if (p1.compareTo(p2) == 0) {
                    throw new java.lang.IllegalArgumentException();
                }
                for (int c3 = c2 + 1; c3 < numPoints; c3++) {
                    Point p3 = points[c3];
                    if (p3 == null) {
                        throw new java.lang.NullPointerException();
                    }
                    if (p1.compareTo(p3) == 0 || p2.compareTo(p3) == 0) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    for (int c4 = c3 + 1; c4 < numPoints; c4++) {
                        Point p4 = points[c4];

                        if (p4 == null) {
                            throw new java.lang.NullPointerException();
                        }

                        // System.out.println(String.format("%s\t%s\t%s\t%s\t", p1, p2, p3, p4));
                        if (p1.compareTo(p4) == 0 
                            || p2.compareTo(p4) == 0 
                            || p3.compareTo(p4) == 0) {
                            throw new java.lang.IllegalArgumentException();
                        }

                        double sl1 = p1.slopeTo(p2);
                        double sl2 = p1.slopeTo(p3);
                        double sl3 = p1.slopeTo(p4);

                        if (sl1 == Double.NEGATIVE_INFINITY 
                            || sl2 == Double.NEGATIVE_INFINITY 
                            || sl3 == Double.NEGATIVE_INFINITY) {
                            throw new java.lang.IllegalArgumentException();
                        }

                        if (sl1 == sl2 && sl1 == sl3) { // all slopes are equals
                            Point _p1 = p1; // left-most point
                            Point _p2 = p1; // right-most point
                            Point[] _points = {p2, p3, p4};
                            for (int c5 = 0; c5 < 3; c5++) {
                                if (_p1.compareTo(_points[c5]) > 0) {
                                    // if current min is greater than a current point
                                    _p1 = _points[c5];
                                }
                                if (_p2.compareTo(_points[c5]) < 0) {
                                    // if current max is less than a current point
                                    _p2 = _points[c5];
                                }
                            }
                            LineSegment l = new LineSegment(_p1, _p2);
                            // System.out.println(l);
                            _segments[numLines++] = l;
                        }
                    }
                }
            }
        }
        segments = new LineSegment[numLines];
        System.arraycopy(_segments, 0, segments, 0, numLines);
    }

    public int numberOfSegments() {
        return segments.length;
    }
    
    public LineSegment[] segments() {
        return segments;
    }

    public static void main(String[] args) {
        // read the N points from a file
        In in = new In(args[0]);
        int N = in.readInt();
        Point[] points = new Point[N];
        for (int i = 0; i < N; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }

        // draw the points
        StdDraw.show(0);
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        BruteCollinearPoints collinear = new BruteCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
    }
}
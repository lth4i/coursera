import edu.princeton.cs.algs4.StdRandom;
import java.util.Arrays;

public class Quicksort {

	private static void exch(Comparable[] a, int i, int j) {
		Comparable temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}

	private static int partition(Comparable[] a, final int lo, final int hi) {
		int i = lo;
		int j = hi + 1;

		while(true) {
			while (a[++i].compareTo(a[lo]) < 0) { // find item on the left to swap
				if (i == hi) break;
			}

			while (a[lo].compareTo(a[--j]) < 0) { // find item on the right to swap
				if (j == lo) break;
			}


			if (j <= i) {
				break; // check if pointer cross
			} else {
				exch(a, i, j); // swap
			}
		}

		exch(a, lo, j); // swap with partition item

		return j; // return index of item known to be in place
	}

	private static void sort(Comparable[] a) {
		StdRandom.shuffle(a);
		sort(a, 0, a.length - 1);
	}

	private static void sort(Comparable[] a, int lo, int hi) {
		if (hi <= lo) return;

		int j = partition(a, lo, hi); // partition
		sort(a, lo, j - 1);	// sort the left
		sort(a, j + 1, hi); // sort the right
	}

	private static void threeWaySort(Comparable[] a, final int lo, final int hi) {
		if (hi <= lo) return;

		int lt = lo;
		int gt = hi;
		Comparable v = a[lo];
		int i = lo;

		while (i <= gt) {
			int cmp = a[i].compareTo(v);
			if (cmp < 0) {
				exch(a, lt++, i++);
			} else if (cmp > 0) {
				exch(a, i, gt--);
			} else {
				i++;
			}
		}

		sort(a, lo, lt - 1);
		sort(a, gt + 1, hi);
	}

	private static Comparable select(Comparable[] a, int k) {
		StdRandom.shuffle(a);
		int lo = 0;
		int hi = a.length - 1;
		while (hi > lo) {
			int j = partition(a, lo, hi);
			if (j < k) {
				lo = j + 1;
			} else if (j > k) {
				hi = j - 1;
			} else {
				return a[k];
			}
		}
		return a[k];
	}

	private static String getString(Comparable[] a) {
		String s = "";
		for (int i = 0; i < a.length; i++) {
			s = s + " " + a[i];
		}
		return s.trim();
	}

	public static void main (String[] args) {
		// int num = 10;
		// String[] a = new String[num];
		// for (int i = 0; i < num; i++) {
		// 	a[i] = i + "";
		// }
		// StdRandom.shuffle(a);
		// System.out.println(Arrays.toString(a));
		// sort(a);
		// System.out.println(Arrays.toString(a));
		String[] a = "49 62 94 22 21 70 52 61 91 84 32 50".split(" ");
		partition(a, 0, a.length - 1);
		System.out.println(getString(a));
		a = "B A B B B A B B A A A A".split(" ");
		partition(a, 0, a.length - 1);
		System.out.println(getString(a));
	}
}
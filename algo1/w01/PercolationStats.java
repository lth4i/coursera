import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {
    private int n, t;

    private final double[] thresholds;

    private final double m, s, l, h;

    public PercolationStats(int N, int T) {
        if (N <= 0 || T <= 0) {
            throw new java.lang.IllegalArgumentException();
        }

        n = N;
        t = T;

        int numCells = n * n;
        thresholds = new double[T];

        for (int c = 0; c < T; c++) {
            Percolation p = new Percolation(N);

            while (!p.percolates()) {
                int i = StdRandom.uniform(N) + 1;
                int j = StdRandom.uniform(N) + 1;
                p.open(i, j);
            }

            int numOpen = 0;
            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= n; j++) {
                    if (p.isOpen(i, j)) {
                        numOpen++;
                    }
                }
            }

            double threshold = (double) numOpen / numCells;
            thresholds[c] = threshold;
        }

        m = StdStats.mean(thresholds);
        s = StdStats.stddev(thresholds);

        l = m - (1.96 * s) / Math.sqrt(t);
        h = m + (1.96 * s) / Math.sqrt(t);
    }
    public double mean() {
        return m;
    }

    public double stddev() {
        return s;
    }

    public double confidenceLo() {
        return l;
    }

    public double confidenceHi() {
        return h;
    }

    public static void main(String[] args) {
        // System.out.println(args);
        int N = Integer.parseInt(args[0]);
        int T = Integer.parseInt(args[1]);

        PercolationStats ps = new PercolationStats(N, T);

        System.out.println(String.format("mean\t\t\t= %s", ps.mean()));
        System.out.println(String.format("stddev\t\t\t= %s", ps.stddev()));
        System.out.println(String.format("95%% confidence interval\t= %s, %s", 
            ps.confidenceLo(), ps.confidenceHi()));
    }
}

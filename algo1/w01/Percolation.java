import java.util.Random;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {

    private final boolean[] cells;

    private final int top, bottom;
    private final int numRealCells;
    private final int length;


    private final WeightedQuickUnionUF uf;

    private final WeightedQuickUnionUF ufTopOnly;

    private int numOpen;

    public Percolation(int N) {

        if (N <= 0) {
            throw new java.lang.IllegalArgumentException();
        }

        length = N;
        numRealCells = N * N;
        numOpen = 0;

        uf = new WeightedQuickUnionUF(numRealCells + 2);
        ufTopOnly = new WeightedQuickUnionUF(numRealCells + 1);

        top = numRealCells;
        bottom = top + 1;

        cells = new boolean[numRealCells];
        for (int i = 0; i < numRealCells; i++) {
            cells[i] = false; // at the beginning, all cells are closed
        }
    }

    private int getCell(int i, int j) {
        if (i < 1 || i > length || j < 1 || j > length) {
            throw new java.lang.IndexOutOfBoundsException();
        }
        int c = (i - 1) * length + (j - 1);
        if (c >= numRealCells) {
            throw new java.lang.IndexOutOfBoundsException(String.format("i=%s, j=%s", i, j));
        } else {
            return c;
        }
    }

    public boolean isOpen(int i, int j) {
        int c = getCell(i, j);
        return cells[c];
    }

    public void open(int i, int j) {
        // System.out.println();
        int c = getCell(i, j);

        if (cells[c]) {
            return; // stop if the cell is already open
        }

        numOpen++;
        cells[c] = true;
        if (c < length) {
            uf.union(top, c);
            ufTopOnly.union(top, c);
        }
        if (c >= numRealCells - length) {
            uf.union(bottom, c);
        }


        if (c % length != 0 && c - 1 >= 0 && cells[c - 1]) { // left
            union(c, c - 1);
        }
        if ((c + 1) % length != 0 && c + 1 < numRealCells && cells[c + 1]) { // right
            union(c, c + 1);
        }
        if (c - length >= 0 && cells[c - length]) { // upper
            union(c, c - length);
        }
        if (c + length < numRealCells && cells[c + length]) { // upper
            union(c, c + length);
        }
        // printRoots();
    }

    private void union(int c1, int c2) {
        uf.union(c1, c2);
        ufTopOnly.union(c1, c2);
    }

    public boolean isFull(int i, int j) {
        int c = getCell(i, j);
        return ufTopOnly.connected(top, c);
    }

    public boolean percolates() {
        return uf.connected(top, bottom);
    }

    private void printGrid() {
        for (int i = 1; i <= length; i++) {
            for (int j = 1; j <= length; j++) {
                int c = getCell(i, j);
                if (isOpen(i, j)) {
                    if (isFull(i, j)) {
                        System.out.print("[ ]");
                    } else {
                        System.out.print("[\u25A0]");
                    }
                } else {
                    System.out.print("[X]");
                }
            }
            System.out.println();
        }
    }

    private void printRoots() {
        System.out.println(String.format("[%s\t]", cells[top]));
        for (int i = 1; i <= length; i++) {
            for (int j = 1; j <= length; j++) {
                int c = getCell(i, j);
                System.out.print(String.format("[%s\t]", cells[c]));
            }
            System.out.println();
        }
    }

    private int getNumOpen() {
        return numOpen;
    }

    private int getNumCells() {
        return numRealCells;
    }

    public static void main(String[] args)  {
        Random r = new Random();

        int N = 1;
        Percolation p = new Percolation(N);
        // p.printGrid();
        p.open(1, 1);
        // for (int i = 0; i < N; i++) {
        //     p.open(i, 0);
        // }
        // while (!p.percolates()) {
        //     int i = r.nextInt(N) + 1;
        //     int j = r.nextInt(N) + 1;
        //     p.open(i, j);
        // }

        // System.out.println();
        // // p.printGrid();
        // p.printGrid();
        // System.out.println((double) p.getNumOpen() / p.getNumCells());
        System.out.println(p.percolates());
        // p.printRoots();
    }

}
public class SeparateChainingHashST<Key, Value> {

	private static class Node {
		private final Object k;
		private Object v;
		private Node next;

		private Node(Object k, Object v, Node next) {
			this.k = k;
			this.v = v;
			this.next = next;
		}
	}

	private int size = 97;
	private Node[] st = new Node[size];

	private int hash(Key k) {
		return (k.hashCode() & 0x7fffffff) % size;
	}

	public Value get(Key k) {
		int i = hash(k);
		for (Node n : st[i]; n != null; n = n.next) {
			if (k.equals(n.k)) return (Value) n.v;
		}
		return null;
	}

	public void put(Key k, Value v) {
		if (Node existingNode = get(k) != null) { // if key is found, update value
			existingNode.v = v;
			return;
		}

		int i = hash(k);
		st[i] = new Node(k, v, st[i]); // create a new node which points to an old one, and put it at the top
	}
	
}
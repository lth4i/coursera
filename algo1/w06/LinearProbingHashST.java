public class LinearProbingHashST<Key, Value> {
	private int size = 30001;
	private Value[] vals = (Value[]) new Object[size];
	private Key[] keys = (Key[]) new Object[size];

	private int hash(Key k) {
		return (k.hashCode() & 0x7fffffff) % size;
	}

	public void put(Key k, Value v) {
		int i;
		for (i = hash(k); keys[i] != null; i = (i + 1) % size) {
			if (keys[i].equals(k)) { // stop if key is found, i.e. update value
				break;
			}
		}

		keys[i] = k;
		vals[i] = v;
	}

	public Value get(Key k) {
		for (int i = hash(k); keys[i] != null; i = (i + 1) % size) {
			if (keys[i].equals(k)) { // key is found
				return vals[i];
			}
		}
		return null;
	}
}
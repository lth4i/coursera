import java.util.Iterator;

public class Deque<Item> implements Iterable<Item> {

    private class Node {
        private final Item item;

        private Node previous;
        private Node next;

        private Node(Item item) {
            this.item = item;
        }
    }

    private class ListIterator implements Iterator<Item> {
        private Node current = first;

        public boolean hasNext() {
            return current != null;
        }

        public void remove() {
            throw new java.lang.UnsupportedOperationException();
        }

        public Item next() {
            if (hasNext()) {
                Item item = current.item;
                current = current.next;
                return item;
            } else {
                throw new java.util.NoSuchElementException();
            }
        }
    }

    private Node first = null;
    private Node last = null;

    private int size;

    public Deque() {
        size = 0;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    public void addFirst(Item item) {
        checkOnAdd(item);
        
        Node oldFirst = first;
        first = new Node(item);

           // point first's next to the old one
           first.next = oldFirst;

        if (last == null) {
            // if there was no first, i.e. empty deque, set last to first
            last = first;
        } else {
            oldFirst.previous = first;
        }
    }

    public void addLast(Item item) {
        checkOnAdd(item);

        Node oldLast = last;
        last = new Node(item);

        // point last's previous to the old one
        last.previous = oldLast;

        if (first == null) {
            // if there was no last, i.e. empty deque, set first to last
            first = last;
        } else {
            oldLast.next = last;
        }
    }

    private void checkOnAdd(Item item) {
        if (item == null) {
            throw new java.lang.NullPointerException();
        } else {
            size++;
        }
    }

    public Item removeFirst() {
        checkOnRemove();

        Node oldFirst = first;
        first = first.next;

        if (first == null) {
            // if there is no more first, i.e. empty deque, set last to null
            last = null;
        } else {
            first.previous = null;
        }

        return oldFirst.item;
    }

    public Item removeLast() {
        checkOnRemove();

        Node oldLast = last;
        last = last.previous;

        if (last == null) {
            // if there is no more last, i.e. empty deque, set first to null
            first = null;
        } else {
            last.next = null;
        }

        return oldLast.item;
    }

    private void checkOnRemove() {
        if (isEmpty()) {
            throw new java.util.NoSuchElementException();
        } else {
            size--;
        }
    }

    public Iterator<Item> iterator() {
        return new ListIterator();
    }

    private String getString() {
        String s = "";
        Iterator i = iterator();
        while (i.hasNext()) {
            s += i.next();
        }
        return s;
    }

    public static void main(String[] args) {
        Deque<String> d = new Deque<String>();

        int num = 10;

        System.out.println("ADD");
        System.out.println(String.format("%s\t%s\t%s", d.size, d.isEmpty(), d.getString()));

        for (int i = 0; i < num; i++) {
            if (i % 2 == 0) {
                d.addFirst(i + "");
            } else {
                d.addLast(i + "");
            }
            System.out.println(String.format("%s\t%s\t%s", d.size, d.isEmpty(), d.getString()));
        }

        System.out.println("REMOVE");
        System.out.println(String.format("%s\t%s\t%s", d.size, d.isEmpty(), d.getString()));

        for (int i = 0; i < num; i++) {
            String item = null;
            if (i % 2 == 0) {
                item = d.removeFirst();
            } else {
                item = d.removeLast();
            }
            System.out.println(String.format("%s\t%s\t%s\t%s", item, d.size, d.isEmpty(), d.getString()));
        }
    }
}
import edu.princeton.cs.algs4.StdIn;

public class Subset {
    public static void main(String[] args) {
        int num = Integer.parseInt(args[0]);

        RandomizedQueue<String> r = new RandomizedQueue<String>();

        String[] strings = StdIn.readAllStrings();

        for (String string : strings) {
            // String[] tokens = string.split(" ");
            r.enqueue(string.trim());

            // for (String t : tokens) {
            //     if (!t.trim().equals("")) {
            //         r.enqueue(t.trim());
            //     }
            // }
        }

        for (int i = 0; i < num; i++) {
            String s = r.dequeue();
            System.out.println(s);
        }
    }
}
import edu.princeton.cs.algs4.StdRandom;
import java.util.Iterator;

public class RandomizedQueue<Item> implements Iterable<Item> {

    private Item[] l;
    private int size = 0;

    private class ListIterator implements Iterator<Item> {
        private int current = size;
        private int[] randomIndices;

        {
            randomIndices = new int[current];
            for (int i = 0; i < current; i++) {
                randomIndices[i] = i;
            }
            StdRandom.shuffle(randomIndices);
        }

        public boolean hasNext() {
            return current > 0;
        }

        public void remove() {
            throw new java.lang.UnsupportedOperationException();
        }

        public Item next() {
            if (hasNext()) {
                return l[randomIndices[--current]];
            } else {
                throw new java.util.NoSuchElementException();
            }
        }
    }

    public RandomizedQueue() {
        l = (Item[]) new Object[1];
    }
    
    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    private void resize(int capacity) {
        Item[] _l = (Item[]) new Object[capacity];
        for (int i = 0; i < size; i++) {
            _l[i] = l[i];
        }
        l = _l;
    }

    public void enqueue(Item item) {
        if (item == null) {
            throw new java.lang.NullPointerException();
        }

        l[size++] = item;

        if (size == l.length) {
            resize(l.length * 2);
        }
    }
    public Item dequeue() {
        if (isEmpty()) {
            throw new java.util.NoSuchElementException();
        }

        int removedIndex = StdRandom.uniform(size);
        Item item = l[removedIndex];

        // for (int i = removedIndex; i < size - 1; i++) {
        //     l[i] = l[i + 1]; // shift the array from the removed index to the last one
        // }

        if (size > 1) {
            l[removedIndex] = l[size - 1]; // swap the remove one with the last one
        }
        

        l[--size] = null;

        if (size == l.length / 4) {
            resize(l.length / 2);
        }

        return item;
    }

    public Item sample() {
        if (isEmpty()) {
            throw new java.util.NoSuchElementException();
        }

        int selectedIndex = StdRandom.uniform(size);
        Item item = l[selectedIndex];

        return item;
    }

    public Iterator<Item> iterator() {
        return new ListIterator();
    }

    private String getString() {
        String s = "";
        Iterator i = iterator();
        while (i.hasNext()) {
            s += i.next();
        }
        return s;
    }

    public static void main(String[] args) {
        RandomizedQueue<String> r = new RandomizedQueue<String>();

        int num = 10;

        System.out.println(String.format("%s\t%s", r.size, r.isEmpty()));
        for (int i = 0; i < num; i++) {
            r.enqueue(i + "");
            System.out.println(String.format("%s\t%s", r.size, r.isEmpty()));
        }

        Iterator i = r.iterator();
        while (i.hasNext()) {
            System.out.println(i.next());
        }

        for (int j = 0; j < num; j++) {
            System.out.println(r.getString());
        }

        while (!r.isEmpty()) {
            String s = r.dequeue();
            System.out.println(String.format("%s\t%s\t%s", s, r.size, r.isEmpty()));
        }
    }
}
public class InsertionSort {

	public static void main(String[] args) {
		String[] tokens = "13 33 51 90 98 87 53 10 11 93".split(" ");
		int[] a = new int[tokens.length];
		for (int i = 0; i < tokens.length; i++) {
			a[i] = Integer.parseInt(tokens[i]);
		}

		printArray(a);
		insertSort(a);
		printArray(a);
	}

	public static void insertSort(int[] a) {
		int counter = 0;
		for (int i = 1; i < a.length; i++) {
			if (a[i] < a[i - 1]) { // if a current item is less than it previously adjacent one
				for (int j = i; j > 0; j--) { // go backward from current position
					if (a[j] < a[j - 1]) {
						swap(a, j, j - 1);
						counter++;
						System.out.println(counter + "\t" + printArray(a));
					}
				}
			}
		}
	}

	public static void selectSort(int[] a) {
		int counter = 0;
		for (int i = 0; i < a.length - 1; i++) {
			int minVal = a[i];
			int minInd = i;
			for (int j = i + 1; j < a.length; j++) {
				if (a[j] < minVal) {
					minVal = a[j];
					minInd = j;
				}
			}
			if (minInd != i) {
				swap(a, i, minInd);
				counter++;
				System.out.println(counter + "\t" + printArray(a));
			}
		}
	}

	public static void swap(int[] a, int i, int j) {
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}

	public static String printArray(int[] a) {
		String s = "";
		for (int i = 0; i < a.length; i++) {
			s = s + " " + a[i];
		}
		return s;
	}
}
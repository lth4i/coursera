public class ResizingArrayQueueOfStrings {
	private String[] s;
	private int first = 0;
	private int last = 0;

	public ResizingArrayQueueOfStrings() {
		s = new String[1];
	}

	public boolean isEmpty() {
		return first == last;
	}

	public void enqueue(String item) {
		s[last++] = item;

		if (last == s.length) {
			resize(s.length * 2);
		}
	}

	public String dequeue() {
		String item = s[first];
		s[first++] = null;

		if (first == s.length / 4) {
			shift();
		}
		if (last == s.length / 4) {
			resize(s.length / 2);
		}

		return item;
	}

	private void resize(int capacity) {
		if (first == 0) {
			String[] newS = new String[capacity];
			for (int i = first; i < last; i++) {
				newS[first] = s[first];
			}
			s = newS;
		} else {

		}
	}

	private void shift() {
		String[] newS = new String[s.length];
		for (int i = first; i < last; i++) {
			newS[i - first] = s[first];
		}
		s = newS;
	}
}
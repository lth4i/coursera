public class LinkedStackOfStrings {

	private class Node {
		String item;
		Node next;
	}

	private Node first = null;

	public void push(String item) {
		Node oldFirst = first;
		first = new Node();
		first.item = item;
		first.next = oldFirst;
	}

	public String pop() {
		Node oldFirst = first.next;
		first = first.next;
		return oldFirst.item;
	}

	public boolean isEmpty() {
		return first == null;
	}
}
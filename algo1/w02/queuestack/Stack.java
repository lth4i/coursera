import java.util.Iterator;

public class Stack<T> implements Iterable<T> {

	private class Node {
		T item;
		Node next;
	}

	private class ListIterator implements Iterator<T> {
		private Node current = first;

		public boolean hasNext() {
			return current != null;
		}

		public void remove() {
			// not supported
		}

		public T next() {
			T item = current.item;
			current = current.next;
			return item;
		}
	}

	private Node first = null;

	public void push(T item) {
		Node oldFirst = first;
		first = new Node();
		first.item = item;
		first.next = oldFirst;
	}

	public T pop() {
		Node oldFirst = first.next;
		first = first.next;
		return oldFirst.item;
	}

	public boolean isEmpty() {
		return first == null;
	}

	public Iterator<T> iterator() {
		return new ListIterator();
	}
}
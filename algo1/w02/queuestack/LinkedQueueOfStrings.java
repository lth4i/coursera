public class LinkedQueueOfStrings {

	private class Node {
		String item;
		Node next;
	}

	private Node first = null;
	private Node last = null;

	public void enqueue(String item) {
		Node oldLast = last;
		last = new Node();
		last.item = item;
		last.next = null;

		if (isEmpty()) {
			first = last;
		} else {
			oldLast.next = last;
		}
	}

	public String dequeue() {
		Node oldFirst = first;
		first = first.next;
		
		if (isEmpty()) { // if the queue is empty, last must be null
			last = null;
		}

		return oldFirst.item;
	}

	public boolean isEmpty() {
		return first == null;
	}

}
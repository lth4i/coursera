package utils

object DataGetter {
  
  def getList: List[BigDecimal] = {
    val lines = scala.io.Source.fromFile("sum.txt").mkString.split("\n").toList
    val list = lines.map(l => new BigDecimal(new java.math.BigDecimal(l)))
    return list//.sortBy(v => v)
  }

  def getGraph: Map[String, List[String]] = {
    val lines = scala.io.Source.fromFile("mincut1.txt").mkString.split("\n").toList

    val adjacentList = lines.foldLeft(Map[String, List[String]]())((_result, line) => {
      val tokens = line.trim.split("\t").toList

      val vertex = tokens.head
      val adjacentVertices = tokens.tail

      _result.updated(vertex, adjacentVertices)
    })

    return adjacentList
  }
  
  def getWeightedGraph: Map[Int, Map[Int, Int]] = {
    val lines = scala.io.Source.fromFile("dijkstraData.txt").mkString.split("\n").toList
    
    val weightedGraph = lines.foldLeft(Map[Int, Map[Int, Int]]())((_result, line) => {
      val tokens = line.trim.split("\t").toList
      val node = tokens.head.toInt
      
      val adjNodes = tokens.tail.map(t => {
        val subTokens = t.split(",")
        val adjNode = subTokens(0).toInt
        val weight = subTokens(1).toInt
        (adjNode, weight)
      }).toMap
      
      _result.updated(node, adjNodes)
    })
    
    return weightedGraph
  }

}
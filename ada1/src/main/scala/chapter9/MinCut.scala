package chapter9

import scala.util.Random
import scala.annotation.tailrec

object MinCut extends App {

  val adjacentList = buildAdjacentList

  //  println(adjacentList.keys.toList.sortBy(node => node))
  //  mergeNodes("57", "84", adjacentList)

    val aList = Map("1" -> List("2", "3", "4"),
        "2" -> List("1", "3", "4", "5"),
        "3" -> List("1", "2", "4"),
        "4" -> List("1", "2", "3"),
        "5" -> List("6", "7", "8", "2"),
        "6" -> List("5", "7", "8"),
        "7" -> List("6", "5", "8"),
        "8" -> List("6", "7", "5"))

//    val (contractedGraph, minCut) =  findMinCut(aList, 1000)
//    println(minCut)
//    println(contractedGraph)
        
//  val nodes = adjacentList.keys.toList.sortBy(node => -adjacentList(node).size)
//  nodes.map(node => {
//    println("%s\t%s".format(node, adjacentList(node).size))
//  })

    val (contractedGraph, minCut) = findMinCut(adjacentList, 100000)
    println(minCut)
    println(contractedGraph)

  def findMinCut(adjacentList: Map[String, List[String]], numTrials: Int): (Map[String, List[String]], Int) = {
    val (contractedGraph, minCut) = (1 to numTrials).toList.foldLeft((Map[String, List[String]](), Integer.MAX_VALUE))((result, num) => {
      val (contractedGraph, minCut) = findMinCut(adjacentList)
      if (minCut < result._2) {
        println(minCut)
        (contractedGraph, minCut)
      } else {
        result
      }
    })
    return (contractedGraph, minCut)
  }

  def findMinCut(adjacentList: Map[String, List[String]]): (Map[String, List[String]], Int) = {
    val contractedGraph = contractGraph(adjacentList)

    val bigNode1 = contractedGraph.keys.toList(0)
    val nodesInGroup1 = bigNode1.split("_").toList
    val bigNode2 = contractedGraph.keys.toList(1)
    val nodesInGroup2 = bigNode2.split("_").toList

    val numCrossLinks = nodesInGroup1.foldLeft(0)((result, node) => {
      val adjNodes = adjacentList(node)
      val numCrossLinkOfNode = adjNodes.foldLeft(0)((_result, adjNode) => {
        if (nodesInGroup2.contains(adjNode)) {
          _result + 1
        } else {
          _result
        }
      })
      result + numCrossLinkOfNode
    })
    return (contractedGraph, numCrossLinks)
  }

  @tailrec
  def contractGraph(adjacentList: Map[String, List[String]]): Map[String, List[String]] = {
    if (adjacentList.size == 2) {
      return adjacentList
    }
    //    println(adjacentList.keys.toList.sortBy(node => node))

    val allNodes = adjacentList.keys.toList
    val node1 = allNodes(Random.nextInt(allNodes.size))
    val node2 = allNodes.filterNot(_ == node1)(Random.nextInt(allNodes.size - 1))

    //    println(node1, node2)

    val newAdjacentList = mergeNodes(node1, node2, adjacentList)

    return contractGraph(newAdjacentList)
  }

  def mergeNodes(node1: String, node2: String, adjacentList: Map[String, List[String]]): Map[String, List[String]] = {
    val newNode = "%s_%s".format(node1, node2)

    // create a list of adjacent vertices of both nodes
    val newAdjacentNodes = (adjacentList(node1) ::: adjacentList(node2)).distinct.filterNot(node => node == node1 || node == node2)

    // remove the 2 merged node from an old adjacent node and add new node
    var updatedAdjacentList = adjacentList.filterKeys(node => node != node1 && node != node2).updated(newNode, newAdjacentNodes)

    //    println(updatedAdjacentList.keys.toList.sortBy(node => node))

    updatedAdjacentList = newAdjacentNodes.foldLeft(updatedAdjacentList)((_result, adjNode) => {
      // for all nodes adjacent to 2 merged node, replace them
      _result(adjNode)
      val newList = newNode :: _result(adjNode).filterNot(node => node == node1 || node == node2)

      _result.updated(adjNode, newList)
    })

    return updatedAdjacentList
  }

  def buildAdjacentList: Map[String, List[String]] = {
    val lines = getData

    val adjacentList = lines.foldLeft(Map[String, List[String]]())((_result, line) => {
      val tokens = line.trim.split("\t").toList

      val vertex = tokens.head
      val adjacentVertices = tokens.tail

      _result.updated(vertex, adjacentVertices)
    })

    return adjacentList
  }

  def getData: List[String] = {
    val strList = scala.io.Source.fromFile("mincut1.txt").mkString.split("\n").toList

    return strList
  }
}
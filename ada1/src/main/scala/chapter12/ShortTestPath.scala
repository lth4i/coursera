package chapter12

import utils.DataGetter

object ShortTestPath extends App {

  val NONE = -1

  val graph = DataGetter.getWeightedGraph

  //  val graph = Map(1 -> Map(2 -> 5, 3 -> 1),
  //      2 -> Map(4 -> 10),
  //      3 -> Map(4 -> 1))

  val nodes = graph.keys.toList.sortBy(node => node)

  //  val destinations = List(7)
  val destinations = List(7, 37, 59, 82, 99, 115, 133, 165, 188, 197)

  val distances = destinations.map(destination => {
    calShortestPath(1, destination, graph)
  }).toList

  println(distances.mkString(","))

  def calShortestPath(source: Int, destination: Int, graph: Map[Int, Map[Int, Int]]): Int = {
    val distancesFromSource = Map(source -> 0)

    return calShortestPath(List(source), destination, distancesFromSource, graph)
  }

  def calShortestPath(exploredNodes: List[Int],
    destination: Int,
    distancesFromSource: Map[Int, Int],
    graph: Map[Int, Map[Int, Int]]): Int = {

    val nearestNode = findNearestAdjacentNode(exploredNodes, distancesFromSource, graph)

    val (fromNode, toNode, distanceFromSource) = nearestNode

    if (toNode == 0) { // terminate if there is no new node
      return 1000000
    }

    //    println("%s\t%s\t%s".format(fromNode, toNode, distanceFromSource))

    //    val distanceFromSource = distancesFromSource(fromNode) + weight

    //    println("%s\t%s".format(toNode, distanceFromSource))

    if (toNode == destination) {
      //      println("Found destination %s".format(destination))
      return distanceFromSource
    }

    val updatedExploredNodes = toNode :: exploredNodes
    val updatedDistancesFromSource = distancesFromSource.updated(toNode, distanceFromSource)

    return calShortestPath(updatedExploredNodes, destination, updatedDistancesFromSource, graph)

  }

  def findNearestAdjacentNode(exploredNodes: List[Int],
    distancesFromSource: Map[Int, Int],
    graph: Map[Int, Map[Int, Int]]): (Int, Int, Int) = {

    val nearestNode = exploredNodes.foldLeft((NONE, NONE, NONE))((currentResult, node) => {

      //      println("The nearest node from %s is %s".format(node, nearestAdjacentNode))

      val nearestAdjacentNode = findNearestAdjacentNode(node, exploredNodes, graph)
      val (adjNode, weight) = nearestAdjacentNode
      val totalWeight = distancesFromSource(node) + weight

      if (adjNode == NONE) { // if there is no unexplored adjacent nodes
        currentResult
      } else if (currentResult._1 == NONE) { // if there is no stored node
        (node, adjNode, totalWeight)
      } else {
        val (_, _, storedWeight) = currentResult
        if (storedWeight > totalWeight) { // if an adjacent node has lower weight
          (node, adjNode, totalWeight)
        } else {
          currentResult
        }
      }
    })

    return nearestNode
  }

  def findNearestAdjacentNode(node: Int, excludedNodes: List[Int], graph: Map[Int, Map[Int, Int]]): (Int, Int) = {
    val adjNodes = graph(node)

    // find the nearest adjacent node of an explored node
    val nearestAdjacentNode = adjNodes.foldLeft((NONE, NONE))((currentResult, adjNodeToWeight) => {
      val (adjNode, weight) = adjNodeToWeight
      //        println(node, pair)
      if (excludedNodes.contains(adjNode)) { // if an adjacent node is already explored
        currentResult
      } else if (currentResult._1 == NONE) { // if there is no stored node
        adjNodeToWeight
      } else {
        val (_, storedWeight) = currentResult
        if (storedWeight > weight) { // if an adjacent node has lower weight
          adjNodeToWeight
        } else { // otherwise, i.e. if an adjacent node has higher weight
          currentResult
        }
      }
    })

    return nearestAdjacentNode
  }
}
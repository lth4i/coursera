package chapter5

import scala.util.Random

object QuickSort extends App {

  val list = getData

  var num = 0;
  //  println(list)
  val pivotSelect = "LAST"
  quickSort(list, pivotSelect)
  println(num)

  //  quickSort(list)

  def quickSort(list: List[Int], pivotSelect: String): List[Int] = {

    if (list.size <= 1) {
      return list
    }

    num = num + list.size - 1

    val pivot = if (pivotSelect.equals("FIRST")) {
      list.head
    } else if (pivotSelect.equals("LAST")) {
      list.last
    } else if (pivotSelect.equals("MEDIAN")) {
      val first = list.head
      val last = list.last
      val middleIndex = if (list.size % 2 == 0) {
        list.size / 2 - 1
      } else {
        list.size / 2
      }
      list(middleIndex)
    } else {
      list(Random.nextInt(list.size))
    }

    var equalOnce = false
    val (left, right) = list.foldLeft((List[Int](), List[Int]()))((result, num) => {
      val (leftPart, rightPart) = result

      if (!equalOnce && num == pivot) {
        // for the first time that the number is equal to pivot, consider it as a pivot
        equalOnce = true
        (leftPart, rightPart)
      } else if (num <= pivot) {
        (num :: leftPart, rightPart)
      } else {
        (leftPart, num :: rightPart)
      }
    })

    return quickSort(left, pivotSelect) ::: (pivot :: quickSort(right, pivotSelect))
  }

  def getData: List[Int] = {
    val strList = scala.io.Source.fromFile("QuickSort.txt").mkString.split("\n")
    val list = strList.map(_.toInt).toList

    return list
  }
}
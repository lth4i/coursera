package chapter1

object MatrixMultiplication extends App {
  
  val size = 2;
  
  val m1 = Map(1 -> Map(1 -> 1, 2 -> 2), 2 -> Map(1 -> 3, 2 -> 4))
  val m2 = Map(1 -> Map(1 -> 1, 2 -> 2), 2 -> Map(1 -> 3, 2 -> 4))
  
  printMatrix(m1)
  println()
  printMatrix(m2)
  println()
  
  val m = multiply(size, m1, m2)
  printMatrix(m)
  
  def printMatrix(m: Map[Int, Map[Int, Int]]) = {
    m.map(row => {
      val (rowIndex, rowVals) = row
      val rowString = rowVals.map(cell => {
        val (_, cellVal) = cell
        cellVal
      }).toList.mkString("\t")
      println(rowString)
    })
  }
  
  def multiply(size: Int, m1: Map[Int, Map[Int, Int]], m2: Map[Int, Map[Int, Int]]): Map[Int, Map[Int, Int]] = {
    val result = (1 to size).map(rowIndex => {
      val rowVals = (1 to size).map(colIndex => {
        val celVal = (1 to size).map(index => {
          m1(rowIndex)(index) * m2(index)(colIndex)
        }).sum
        (colIndex -> celVal)
      }).toMap
      (rowIndex -> rowVals)
    }).toMap
    
    
    return result
  }
}
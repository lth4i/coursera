package chapter1

object KaratsubaMultiplication extends App {

  val num1 = 123
  val num2 = 456

  println(multi(num1, num2))
  println(num1 * num2)

  def multi(num1: Int, num2: Int): Int = {
    println("\t%s\t%s".format(num1, num2))
    val num1Str = num1.toString
    val num2Str = num2.toString

    val size1 = num1Str.size
    val size2 = num2Str.size
    val size = List(size1, size2).max

    if (size1 == 1 || size2 == 1) {
      println("\t\t%s * %s = %s".format(num1, num2, num1 * num2))
      return num1 * num2
    }

    val (num1Part1Str, num1Part2Str) = if (size % 2 == 0) num1Str.splitAt(size / 2) else num1Str.splitAt(size / 2 + 1)
    val (num2Part1Str, num2Part2Str) = if (size % 2 == 0) num2Str.splitAt(size / 2) else num2Str.splitAt(size / 2 + 1)

    val num1Part1 = num1Part1Str.toInt
    val num1Part2 = num1Part2Str.toInt
    val num2Part1 = num2Part1Str.toInt
    val num2Part2 = num2Part2Str.toInt

    val numZeroes = size / 2

    val ac = multi(num1Part1, num2Part1)
    val bd = multi(num1Part2, num2Part2)
    val middle = multi(num1Part1 + num1Part2, num2Part1 + num2Part2)

    println(size, numZeroes)
    println("%s * %s = %s + %s + %s = %s"
      .format(num1, num2,
        ac * Math.pow(10, size).toInt, bd, (middle - ac - bd) * Math.pow(10, numZeroes).toInt,
        ac * Math.pow(10, size).toInt + bd + (middle - ac - bd) * Math.pow(10, numZeroes).toInt))

    return ac * Math.pow(10, size).toInt + bd + (middle - ac - bd) * Math.pow(10, numZeroes).toInt
  }

}
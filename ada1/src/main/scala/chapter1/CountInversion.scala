package chapter1

import scala.annotation.tailrec

object CountInversion extends App {

//  val list = List.range(1000, 0, -1)
  val list = getData
  println(list.size)
//  println(list)
  val (_, num) = mergeSort(list)
  println(num)
  println(countInversionSlow(list))
  
  def getData: List[Int] = {
    val strList = scala.io.Source.fromFile("IntegerArray.txt").mkString.split("\n")
    val list = strList.map(_.toInt).toList
    
    return list
  }
  
  def mergeSort(list: List[Int]): (List[Int], Double) = {
    val size = list.size
    
    if (size == 1) { // if a list only has one element, return it
      return (list, 0)
    }
    
    // split a list into two parts and sort them
    val (firstHalf, secondHalf) = list.splitAt(size / 2)
    val (sortedFirstHalf, numLeftInversions) = mergeSort(firstHalf)
    val (sortedSecondHalf, numRightInversions) = mergeSort(secondHalf)
    
    val (sortedList, numSplitInversions) = merge(sortedFirstHalf, sortedSecondHalf, List(), 0)
    
    // merge two SORTED parts together
    return (sortedList, numLeftInversions + numRightInversions + numSplitInversions)
  }
  
  @tailrec
  def merge(firstHalf: List[Int], secondHalf: List[Int], sortedList: List[Int], numInversion: Double): (List[Int], Double) = {
    if (firstHalf.isEmpty) { // return all second half if the first half is empty
      return (sortedList ::: secondHalf, numInversion)
    } else if (secondHalf.isEmpty) { // return all first half if the second half is empty
      return (sortedList ::: firstHalf, numInversion)
    } else {
      val firstHalfHead = firstHalf.head
      val secondHalfHead = secondHalf.head
      
      if (firstHalfHead <= secondHalfHead) {
    	// if the first element of the first half is less than or equal the first element of the second half,
        // put it at the beginning and continue with the remaining of the first half and all the second half
        val newSortedList = sortedList :+ firstHalfHead
        return merge(firstHalf.tail, secondHalf, newSortedList, numInversion)
      } else {
        // otherwise put the first element of the second half at the beginning 
        // and continue with all the first half and the remaining of  the second half
        val newSortedList = sortedList :+ secondHalfHead
        return merge(firstHalf, secondHalf.tail, newSortedList, numInversion + firstHalf.size)
      }
    }
  }
  
  def countInversionSlow(list: List[Int]): Int = {
    var count = 0
    (0 until list.size - 1).map(num1 => {
      (num1 + 1 until list.size).map(num2 => {
        if (list(num1) > list(num2)) {
          count +=1
        }
      })
    })
    return count
  }
}
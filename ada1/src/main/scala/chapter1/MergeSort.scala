package chapter1

import scala.annotation.tailrec

object MergeSort extends App with Sort {

  val list = (1 to 1000000).toList.reverse
  
  val l = List(5,3,8,9,1,7,0,2,6,4)
  
  val sortedList = sort(l)

  
  override def sort(list: List[Int]): List[Int] = {
    val size = list.size
    
    if (size == 1) { // if a list only has one element, return it
      return list
    }
    
    // split a list into two parts and sort them
    val (firstHalf, secondHalf) = list.splitAt(size / 2)
    val sortedFirstHalf = sort(firstHalf)
    val sortedSecondHalf = sort(secondHalf)
    
    println("%s".format((sortedFirstHalf ::: sortedSecondHalf).mkString(", ")))
    
    // merge two SORTED parts together
    return merge(sortedFirstHalf, sortedSecondHalf, List())
  }
  
  @tailrec
  def merge(firstHalf: List[Int], secondHalf: List[Int], sortedList: List[Int]): List[Int] = {
    if (firstHalf.isEmpty) { // return all second half if the first half is empty
      return sortedList ::: secondHalf
    } else if (secondHalf.isEmpty) { // return all first half if the second half is empty
      return sortedList ::: firstHalf
    } else {
      val firstHalfHead = firstHalf.head
      val secondHalfHead = secondHalf.head
      
      if (firstHalfHead <= secondHalfHead) {
    	// if the first element of the first half is less than or equal the first element of the second half,
        // put it at the beginning and continue with the remaining of the first half and all the second half
        val newSortedList = sortedList :+ firstHalfHead
        return merge(firstHalf.tail, secondHalf, newSortedList)
      } else {
        // otherwise put the first element of the second half at the beginning 
        // and continue with all the first half and the remaining of  the second half
        val newSortedList = sortedList :+ secondHalfHead
        return merge(firstHalf, secondHalf.tail, newSortedList)
      }
    }
  }
}
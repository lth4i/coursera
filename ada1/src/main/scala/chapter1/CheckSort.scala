package chapter1

import java.util.Date
import scala.util.Random

object CheckSort extends App {

  val list = Random.shuffle((1 to 10000).toList)
  
  println("Merge Sort: %s".format(checkSort(MergeSort, list)))
  println("Bubble Sort: %s".format(checkSort(BubbleSort, list)))
  
  def checkSort(sort: Sort, list: List[Int]): Long = {
    val start = getTime
    sort.sort(list)
    val end = getTime
    return end - start
  }
  
  def getTime: Long = {
    val date = new Date()
    return date.getTime()
  }
}
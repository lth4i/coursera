package chapter1

import scala.annotation.tailrec
import scala.util.Random

object BubbleSort extends App with Sort {

  val list = Random.shuffle((1 to 1000).toList)
  
  println(list)
  println(sort(list))
  
  override def sort(list: List[Int]): List[Int] = {
    return sort(list, 0, list.size - 1)
  }
  
  @tailrec
  def sort(list: List[Int], currentIndex: Int, upperIndex: Int): List[Int] = {

    if (upperIndex <= 0) {
      return list
    } else if (currentIndex == upperIndex) {
      return sort(list, 0, upperIndex - 1)
    } else {

      val currentNum = list(currentIndex)
      val nextNum = list(currentIndex + 1)

      val updatedList = if (currentNum > nextNum) {
        swap(list, currentIndex)
      } else {
        list
      }

      return sort(updatedList, currentIndex + 1, upperIndex)
    }
  }

  def swap(list: List[Int], currentIndex: Int): List[Int] = {

    if (currentIndex > list.size - 2) {
      throw new Exception("The given list has the size of %s hence the current index, which is %s, has to be lower than %s"
        .format(list.size, currentIndex, list.size - 2))
    }

    val (left, tempRight) = list.splitAt(currentIndex)

    val currentItem = tempRight(0)
    val nextItem = tempRight(1)
    val (_, right) = tempRight.splitAt(2)

    return left ::: (nextItem :: (currentItem :: right))
  }
}
package chapter1

trait Sort {
  def sort(list: List[Int]): List[Int]
}
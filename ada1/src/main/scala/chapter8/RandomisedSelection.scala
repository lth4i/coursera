package chapter8

import scala.util.Random

object RandomisedSelection extends App {

  val list = Random.shuffle((1 to 100).toList)
  
//  println(list)
  
  
//    println(3,  select(list, 3))
  
  (1 to 100).map(num => {
    val loc = select(list, num, 1)
    println(num, loc)
  })
  
  def select(list: List[Int], orderStatistic: Int, stepCount: Int): Int = {
    
//    println(list, orderStatistic)
    
    if (orderStatistic < 1 || orderStatistic > list.size) {
      throw new Exception("Invalid order statistic [%s], [%s]".format(orderStatistic, list.size))
    }
    
    val pivot = list(Random.nextInt(list.size))
    
//    println(pivot)
    
    var equalFound = false
    
    val (left, right) = list.foldLeft((List[Int](), List[Int]()))((_result, item) => {
      val (left, right) = _result
      
      if (item > pivot) {
        (left, item :: right)
      } else {
        if (!equalFound && item == pivot) {
          equalFound == true
          _result
        } else {
          (item :: left, right)
        }
      }
    })
    
//    println(left, right)
    
    if (left.size == orderStatistic - 1) {
      println(stepCount)
      return pivot
    } else if (left.size >= orderStatistic) {
      return select(left, orderStatistic, stepCount + 1)
    } else {
      val newOrderStatistic = orderStatistic - left.size - 1
      return select(right, newOrderStatistic, stepCount + 1)
    }
  }
}
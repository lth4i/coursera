package misc

import scala.annotation.tailrec

object BinarySearch extends App {

  val list = List.range(0, 101, 2)
  println(list)

  (0 to 100).map(num => {
    println("[%s]\t[%s]".format(num, searchIndex(num, list)))
  })

  @tailrec
  def search(value: Int, list: List[Int]): Boolean = {
    val size = list.size

    if (size == 0) {
      return false
    } else if (size == 1) {
      return list.head == value
    }

    val middleElement = list(size / 2)

    if (middleElement == value) {
      return true
    }

    val remainingList = if (middleElement > value) {
      list.slice(0, size / 2)
    } else {
      list.slice((size / 2) + 1, size)
    }

    return search(value, remainingList)
  }

  def searchIndex(value: Int, list: List[Int]): Int = {
    return searchIndex(value, list, 0, list.size)
  }
  
  @tailrec
  def searchIndex(value: Int, list: List[Int], startIndex: Int, endIndex: Int): Int = {
    
    if (startIndex > endIndex) {
      return -1
    } else if (startIndex == endIndex) {
      if (list(startIndex) == value) {
        return startIndex
      } else {
        return -1
      }
    }
    
    val middleIndex = (startIndex + endIndex) / 2
    val middleElem = list(middleIndex)
    
    if (middleElem == value) {
      return middleIndex
    }
    
    val (newStartIndex, newEndIndex) = if (middleElem > value) {
      (startIndex, middleIndex - 1)
    } else {
      (middleIndex + 1, endIndex)
    }
    
    return searchIndex(value, list, newStartIndex, newEndIndex)
  }
}
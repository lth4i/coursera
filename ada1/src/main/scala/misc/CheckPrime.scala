package misc

import java.util.Calendar

object CheckPrime extends App {

  val list = (2 to 10000).toList
  
  var start = Calendar.getInstance().getTime().getTime()
  val primes1 = getPrimes(list)
  var end = Calendar.getInstance().getTime().getTime()
  println(end - start)
  
  start = Calendar.getInstance().getTime().getTime()
  val primes2 = getPrimesOld(list)
  end = Calendar.getInstance().getTime().getTime()
  println(end - start)
  
  println(primes1 == primes2)
    
  def getPrimes(list: List[Int], currentPrime: Int=2, allPrimes: List[Int]=List()): List[Int] = {
    
    val newAllPrimes = allPrimes :+ currentPrime
    
    val remainingList = (1 to list.last / currentPrime).toList.foldLeft(list)((_remainingList, num) => {
      _remainingList.filterNot(_ == currentPrime * num)
    })
    
    if (remainingList.isEmpty) {
      return newAllPrimes
    }
    
    val nextPrime = remainingList.head
    return getPrimes(remainingList, nextPrime, newAllPrimes)
  }
  
  def getPrimesOld(list: List[Int]): List[Int] = {
    val allPrimes = list.foldLeft(List[Int]())((_result, num) => {
      if (isPrime(num)) {
        _result :+ num
      } else {
        _result
      }
    })
    return allPrimes
  }
  
  def isPrime(num: Int): Boolean = {
    (2 to Math.sqrt(num).toInt).map(_num => {
      if (num % _num == 0) {
//        println("[%s] is not prime".format(num))
        return false
      }
    })
    return true
  }
}
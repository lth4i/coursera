package misc

object ReverseList extends App {
  
  val list = (1 to 10).toList
  println(list)
  println(reverseList(list))
  
  
  def reverseList(list: List[Int]): List[Int] = list match {
    case element :: elements => {
      return reverseList(elements) :+ element
    }
    case List() => {
      return List()
    }
  }
}
package chapter14

import utils.DataGetter
import scala.annotation.tailrec

object TwoSum extends App {

//  val l = List(0,1,3,4,6,7,8,10)
//  println(shrink(l, 1))

  //  data.keys.toList.sortBy(v => v).map(v => {
  //    if (data(v) > 1) println("%s\t%s".format(v, data(v)))
  //  })

//  val total = cal2Sum(-200, 200, data)
  val total = cal2Sum(-10000, 10000)
  println(total)

  def buildData: Map[BigDecimal, Int] = {
    val l = DataGetter.getList

//    val l = (-100 to 100).toList :+ 100
    
    val valToNumOccurence = l.foldLeft(Map[BigDecimal, Int]())((result, num) => {
      if (result.contains(num)) {
        result.updated(num, result(num) + 1)
      } else {
        result.updated(num, 1)
      }
    })

    return valToNumOccurence
  }

  def cal2Sum(start: Int, end: Int): Int = {
    val data = buildData
    val vals = data.keys.toList.sortBy(v => v)
//    println(vals.head)
//    println(vals.last)
//    println(vals.last - vals.head)
//    if (true) return 0
    
    val shrinkedVals = shrink(vals, end - start + 1)
    
    val size = vals.size
    val shrinkedSize = shrinkedVals.size
    
    println(size)
    println(shrinkedSize)
    if (true) return 0
    
    val total = (start to end).toList.foldLeft(0)((total, num) => {
      val checked = cal2Sum(num, 0, size, vals, data)
      if (checked) {
        total + 1
      } else {
        total
      }
    })
    return total
  }
  
  @tailrec
  def cal2Sum(sum: BigDecimal, curCounter: Int, size: Int, vals: List[BigDecimal], data: Map[BigDecimal, Int]): Boolean = {
    if (curCounter == size) {
      return false
    }
    
    val curVal = vals(curCounter)
    val curVal1 = sum - curVal
    
    if (curVal != curVal1 && data.contains(curVal1)) {
      println("%s = %s + %s".format(sum, curVal, curVal1))
      return true
    }
    
    if (curVal == curVal1 && data(curVal) > 1) {
      println("%s = %s * 2".format(sum, curVal))
      return true
    }
    
    return cal2Sum(sum, curCounter + 1, size, vals, data)
  }
  
  def shrink(list: List[BigDecimal], range: Int): List[BigDecimal] = {
    return shrink(List(), 0, list.size, list, range)
  }
  
  @tailrec
  def shrink(updatedList: List[BigDecimal], currentCounter: Int, size: Int, list: List[BigDecimal], range: Int): List[BigDecimal] = {
    if (currentCounter >= size - 2) {
      return updatedList.reverse
    }
    
    val curItem = list(currentCounter)
    val nextItem = list(currentCounter + 1)
    
    if (nextItem - curItem > range) {
      shrink(updatedList, currentCounter + 1, size, list, range)
    } else {
      println("%s\t%s".format(currentCounter, curItem))
      println("%s\t%s".format(currentCounter+1, nextItem))
      shrink(nextItem :: (curItem :: updatedList), currentCounter + 2, size, list, range)
    }
  }
}
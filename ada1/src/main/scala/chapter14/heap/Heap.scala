package chapter14.heap

abstract class Heap(val items: List[Int]) {
  def add(num: Int): Heap
//  def add(num: Int): Heap = {
//    if (isNewRoot(num)) {
//      return null
//    }
//    
//    null
//  }
//  
//  def isNewRoot(num: Int): Boolean
//  
//  def getNewRoot(items: List[Int]): Int
  
  val size = items.size
  
  def getChildrent(index: Int): List[Int] = {
    if (index > size) {
      throw new IndexOutOfBoundsException("%s".format(index))
    }
    
    val cIndex1 = index * 2 + 1
    
    if (cIndex1 >= size) {
      return List()
    } else if (cIndex1 == size - 1) {
      return List(items(cIndex1))
    } else {
      val cIndex2 = cIndex1 + 1
      return List(items(cIndex1), items(cIndex2))
    }
  }
}
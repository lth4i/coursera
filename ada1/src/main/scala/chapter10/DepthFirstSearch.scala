package chapter10

object DepthFirstSearch {

  def depthFirstSearch(graph: Map[String, List[String]],
    stack: List[String],
    knownNodes: List[String]): List[String] = {

    if (stack.isEmpty) {
      return knownNodes
    }

    val node = stack.last
    val remainingStack = stack.init

    val adjacentNodes = graph(node)

    val (updatedKnownNodes, updatedStack) = adjacentNodes.foldLeft((knownNodes, remainingStack))((_result, node) => {
      val (_knownNodes, _stack) = _result
      if (_knownNodes.contains(node)) {
        _result
      } else {
        (_knownNodes :+ node, _stack :+ node)
      }
    })

    return depthFirstSearch(graph, updatedStack, updatedKnownNodes)
  }
}
package chapter10

import scala.collection.mutable.Queue
import scala.util.Random
import utils.DataGetter

object BreadthFirstSearch extends App {
  
  val graph = DataGetter.getGraph
  
  val nodeToLayer = breadthFirstSearch(graph)
  
  val layerToNodes = nodeToLayer.foldLeft(Map[Int, List[String]]())((_result, pair) => {
    val (node, layer) = pair
    if (_result.contains(layer)) {
      val nodes = _result(layer)
      _result.updated(layer, node :: nodes)
    } else {
      _result.updated(layer, List(node))
    }
  })
  
  val layers = layerToNodes.keys.toList.sortBy(layer => layer)
  
  layers.map(layer => {
    println("%s\t%s".format(layer, layerToNodes(layer)))
  })
  
//  println(nodeToLayer )
  
  def breadthFirstSearch(graph: Map[String, List[String]]): Map[String, Int] = {
    val randomNode = graph.keys.toList(Random.nextInt(graph.keys.size))
    
    val nodeToLayer = breadthFirstSearch(graph, List(randomNode), 1, Map(randomNode -> 0))
    
    return nodeToLayer
  }
  
  def breadthFirstSearch(graph: Map[String, List[String]],
      queue: List[String],
      currentLayer: Int,
      nodeToLayer: Map[String, Int]): Map[String, Int] = {
    
    if (queue.isEmpty) {
      return nodeToLayer
    }
    
    val node = queue.head
    val remainingQueue = queue.tail
    
    val adjacentNodes = graph(node)
    
    val (updatedNodeToLayer, updatedQueue) = adjacentNodes.foldLeft((nodeToLayer, remainingQueue))((_result, aNode) => {
      val (_nodeToLayer, _queue) = _result
      if (_nodeToLayer.contains(aNode)) {
        _result
      } else {
        (_nodeToLayer.updated(aNode, currentLayer), _queue :+ aNode)
      }
    })
    
    return breadthFirstSearch(graph, updatedQueue, currentLayer + 1 , updatedNodeToLayer)
  }
}
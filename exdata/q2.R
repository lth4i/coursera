NEI <- readRDS("summarySCC_PM25.rds")

# filter Baltimore data
b_NEI <- NEI[NEI$fips == "24510",]

# calculate total emission per year
b_ey <- with(b_NEI, tapply(Emissions, year, FUN=sum))

png("plot2.png",  width = 480, height = 480, units = "px")

plot(b_ey ~ names(b_ey), xlab="Year", ylab="Total Emission", main="Total Emission In Baltimore Per Year")
lines(b_ey ~ names(b_ey))

dev.off()
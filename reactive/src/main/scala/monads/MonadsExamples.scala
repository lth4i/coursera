package monads

object MonadsExamples extends App {

  
  val l = (1 to 10).toList
  
  val l1 = l.flatMap(i => (1 to i))
  
  println(l1)
}
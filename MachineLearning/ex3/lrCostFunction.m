function [J, grad] = lrCostFunction(theta, X, y, lambda)
%LRCOSTFUNCTION Compute cost and gradient for logistic regression with 
%regularization
%   J = LRCOSTFUNCTION(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================

z = X * theta;

h = sigmoid(z);

tail_theta = theta(2:length(theta), 1);

J = (1 / m) * (-(y' * log(h)) - ((1 - y)' * log(1 - h))) + (lambda / (2 * m)) * (tail_theta' * tail_theta) ;


errors = h - y;
products = errors' * X;
temp_grad = ((1 / m) * products)';
grad(1) = temp_grad(1);
grad(2:length(theta), 1) = temp_grad(2:length(theta), 1) + (lambda / m).* tail_theta;

grad = grad(:);

end

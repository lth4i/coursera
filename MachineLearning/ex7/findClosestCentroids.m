function idx = findClosestCentroids(X, centroids)
%FINDCLOSESTCENTROIDS computes the centroid memberships for every example
%   idx = FINDCLOSESTCENTROIDS (X, centroids) returns the closest centroids
%   in idx for a dataset X where each row is a single example. idx = m x 1 
%   vector of centroid assignments (i.e. each entry in range [1..K])
%

% Set K
K = size(centroids, 1);

% You need to return the following variables correctly.
idx = zeros(size(X,1), 1);

% ====================== YOUR CODE HERE ======================
% Instructions: Go over every example, find its closest centroid, and store
%               the index inside idx at the appropriate location.
%               Concretely, idx(i) should contain the index of the centroid
%               closest to example i. Hence, it should be a value in the 
%               range 1..K
%
% Note: You can use a for-loop over the examples to compute this.
%

% for i = 1:size(X,1)
% 	x = X(i,:);
% 	curr_distance = Inf;
% 	curr_closest = 0;
% 	for j = 1:K
% 		c = centroids(j,:)
% 		distance = (x - j) * (x - j)'
% 		if distance < curr_distance
% 			curr_distance = distance;
% 			curr_closest = j;
% 		end
% 	end
% 	idx(i,:) = curr_closest;
% end

distances = zeros(size(X,1), K);
size(distances)
for k = 1:K
	c = centroids(k, :);
	% size(c)
	% size(X)
	distance = sum((X - (ones(size(X,1), 1) * c)).^2, 2);
	% size(distance)
	distances(:, k) = distance;
	% V * P
end

[v, p] = min(distances, [], 2);

idx = p;

% =============================================================

end

